# Japanese Walnut Genome Assembly, Annotation & Comparative Genomics

[TOC]

# Important files

### Project Directory
- **Project Directory:** ```/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/``` 


### Prior to Assembly
- **Raw Reads:** ```/archive/projects/PromethION/plant/JWalnut_assembly_files/Juglans_ailantifolia_pass.fastq.tar.gz ```

- **Filtered Raw Reads:** ```/archive/projects/PromethION/plant/JWalnut_assembly_files/filtered_Juglans_ailanthifolia.fasta.tar.gz ```

### Assembly Final Read
- **Final Assembly:** ```/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/09_scaffolding/ragtag_output/UCONN_J_ailantifolia_1_0_0.fasta```

### Final Annotation 
- **Final Annotation:** ```/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/annotation/05_EASEL/j_ailantifolia/final_predictions/ailantifolia_filtered.gff```

### Datasheets

- **Assembly:** https://docs.google.com/spreadsheets/d/1_0dHiubPZctuajOWmCaoaL-j_iQr8_8H/edit?usp=sharing&ouid=111910649726359221978&rtpof=true&sd=true 

- **Annotation:** https://docs.google.com/spreadsheets/d/15VHo4MRaKaHhGPvuav1UQJL7snZYypJS/edit?usp=sharing&ouid=111910649726359221978&rtpof=true&sd=true 


# Introduction

![Introductory_image](Figures/Introductory_Image.png)
> **Range Map, Phylogeny & Species:** The figure above depicts a range map of various members of the genus *Juglans* along with a phylogeny and an image of a flowering *Juglans ailantifolia* leaf. **Panel A** came from work described in [Likhanov, F., et al., 2020](https://www.phytomorphology.com/articles/identifying-species-and-hybrids-in-the-genus-juglans-by-biochemical-profiling-of-bark-45418.html). **Panel B** depicts a Family tree of different walnut species comparing similarities of nuSSR's from BlASTN results by [Ebrahimi, Aziz, et al, 2019](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6403238/). Lastly, **Panel C** shows a flowering *Juglans ailantifolia* leaf; Image was obtained through: Plant Image Library from Boston, USA, CC BY-SA 2.0 <https://creativecommons.org/licenses/by-sa/2.0>, via [Wikimedia Commons](https://upload.wikimedia.org/wikipedia/commons/1/1c/Juglans_ailantifolia_%28Japanese_Walnut%29_%2834891906165%29.jpg)

As a continuation of the Butternut Genome Project, where the first reference genome for *Juglans cinerea* was generated, our team sought to further investigate disease resistance in Butternut. To achieve this, we turned our attention to *Juglans ailantifolia*, commonly known as Japanese Walnut, a closely related species with natural resistance to the invasive fungus *Ophiognomonia clavigignenti-juglandacearum* (OC-J). By understanding the genomic components that confer this resistance in Japanese Walnuts, we aim to identify similar patterns in butternut populations to aid conservation efforts.

Leveraging long-read Oxford Nanopore sequencing technology, we constructed the first reference genome for Japanese Walnut. This foundational work set the stage for our search for resistance mechanisms within the genome, with the ultimate goal of applying these findings to enhance the resilience of butternut trees.

Our methodology involved sequencing, assembling, and annotating the genome of *J. ailantifolia*. Using bioinformatics tools such as Canu and RagTag, we achieved a high-quality, chromosome-level assembly of 527 Mb. The assembly's completeness was validated with BUSCO analysis against the *Embryophyta* database, revealing an initial score of 99.0% and about 213x coverage. Further refinement through Purge and RagTag maintained a high BUSCO score of 98.9%.

Gene annotation, performed using EASEL and aligned RNA evidence, identified 26,213 filtered genes and achieved a BUSCO completeness score of 97.7%. Comparative genomics analysis with 11 closely related *Juglandaceae*, *Alfaropsis roxburghiana*, *Carya illinoinensis*, *Cyclocarya paliurus*, *Juglans cinerea*, *Juglans hindsii*, *Juglans mandshurica*, *Juglans microcarpa x regia*, *Juglans nigra*, *Juglans regia*, *Pterocarya stenoptera*, and *Platycarya strobilacea*, were used to identified potential gene families candidates associated with disease resistance.

Additionally, we analyzed tandem repeats, methylation patterns, transposable elements, and synteny to gain deeper insights into the evolutionary processes in the Japanese walnut genome and, therefore, point out or identify potential genomic patterns related to resistance.


## DNA Extration and Sequencing
![652b823fa578d9a337027972_1000x1000](/uploads/48cf2140e1ef5d46f784424c79632a8d/652b823fa578d9a337027972_1000x1000.png)

High molecular weight (HMW) gDNA was extracted from *Juglans ailanthifolia* leaves using a modified Vaillancourt protocol and sequenced with the PromethION ONT sequencer. This sequencer was selected based on its long-read sequencing and real-time data generation features, enabling high-quality genomic data with streamlined library preparation.

### ONT Chemistry Model
- **Flow Cell Type:** FLO-PRO114M
- **Flow Cell ID:** PAO84651
- **Kit Type:** SQK-LSK114

### Sequencing Reports
- [Report_1](https://plantgenomicslab.gitlab.io/Japanese-walnut-assembly-and-annotation/report_PAO84651_20230627_1518_290b9714.html)

- [Report_2](https://plantgenomicslab.gitlab.io/Japanese-walnut-assembly-and-annotation/report_PAO84651_20230705_1113_eaff3c5a.html)

# License
Open Source Project provided by the Plant Computational Genomics Lab (Wegrzyn Lab) at the University Of Connecticut.
https://plantcompgenomics.com/


# Data Availability 

The reference genome data for *Juglans ailantifolia* (Japanese Walnut) generated in this study are publicly available in the National Center for Biotechnology Information (NCBI) database under BioProject accession number **PRJNA1055594**. Raw sequencing reads have been deposited in the NCBI Sequence Read Archive (SRA) under accession numbers SRR28392653 and **SRR28392652**. The genome assembly is available in the NCBI Genome Archive under accession number **JBEUSA000000000**. Gene annotations, assembly metrics, and associated metadata can be accessed through Zenodo (DOI: ZZZZZZZ-coming soon).

Comparative genomics data, including the genomes of the 11 Juglandaceae family members (*Alfaropsis roxburghiana*, *Carya illinoinensis*, *Cyclocarya paliurus*, *Juglans cinerea*, *Juglans hindsii*, *Juglans mandshurica*, *Juglans microcarpa x regia*, *Juglans nigra*, *Juglans regia*, *Pterocarya stenoptera*, and *Platycarya strobilacea*), are available in NCBI from their respective Bioproject submissions and accession numbers as cited in the manuscript.


# Publication
- Coming soon!

# Authors
Karl C. Fetter, Cristopher R. Guzman-Torres, Stefan Wnuk, Amanda Mueller, Harshita Akella, Keertana Chagari, Anthony He, Emily Strickland, Emily Trybulec, David Baukus, Laurel Humphrey, Owen McEwing, Cynthia N. Webster, Michelle L. Neitzey, Nicole Pauloski, Aziz Ebrahimi, Emry Brannan, Martin Williams, Jeanne Romero-Severson, Sean Hoban, Keith Woeste, Carolyn C. Pike, Rachel J. O’Neill, Jill L. Wegrzyn


# Acknowledgement

# Helpful Resources

- Meet our Lab: https://plantcompgenomics.com/ 

- Uconn Computational Biology Core Tutorials:
https://bioinformatics.uconn.edu/resources-and-events/tutorials-2/ 

- Assembly Terminology: https://www.ncbi.nlm.nih.gov/grc/help/definitions/ 

# Cite As

# Next Page
- **Next Page:** [Assembly](https://gitlab.com/PlantGenomicsLab/Japanese-walnut-assembly-and-annotation/-/blob/main/README_Assembly.md?ref_type=heads) 

# Refrences
- 
