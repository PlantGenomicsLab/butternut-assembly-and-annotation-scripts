# Japanese Walnut Genome Assembly

[TOC]

# Assembly Process
![Genome_Assembly_WorkFlow](/uploads/4db27d061dddaa497ad1340a5b7e0173/Genome_Assembly_WorkFlow.png)

The genome assembly process is divided into six key stages, with intermediate steps for quality control and validation at each phase, as illustrated in the figure. While the workflow may seem straightforward, the complexity can vary significantly depending on project goals and the methods chosen. Nevertheless, this process plays a crucial role in translating the genetic makeup of a species into a resource that can reveal its evolutionary history, genetic diversity, and biological complexity. Using molecular and bioinformatic approaches, scientists can unlock valuable insights into the species' underlying genetic architecture.

## 01. Oxford Nanopore Reads

### Nanoplot

| NanoPlot            | Raw Reads          |
|---------------------|--------------------|
| Mean read length    | 11,200.8           |
| Mean read quality   | 18.7               |
| Median read length  | 8,682.0            |
| Median read quality | 19.2               |
| Number of reads     | 11,472,949.0       |
| Read length N50     | 19,115.0           |
| STDEV read length   | 10,874.4           |
| Total bases         | 128,506,025,356.0  |


![652d6be46dcbcd4838e1ba6f_1000x1000](/uploads/cb4513ba0bf55a32ab000e0c9ed4f576/652d6be46dcbcd4838e1ba6f_1000x1000.png)

Nanoplot is a tool used for visualizing and processing long-read sequencing data. It provides flexible plotting tools to assess assembly quality. One crucial metric is read length, which indicates the number of fragments in the assembly and reflects the accuracy of base calls. The read length vs. average read quality plot shows higher quality scores between 15 to 25, suggesting good-quality data was generated. However, there are outliers around the 500,000 mark that may need further investigation. 

> 🔧 **Troubleshooting:** During the process, there were some challenges with formatting the script and creating a nanoplot table using Markdown. To address the incorrect formatting in the original script, we focused on eliminating white space, and with the help of Google, we managed to keep track of all the proper Markdown tools needed for making the table. Nevertheless, nanoplot was successfully used to analyze *J.ailantifolia* reads and obtain the required data.


- **Full Script:** [Nanoplot_rawreads.sh](Assembly Scripts/01_Raw_Reads/Nanoplot_rawreads.sh)

### Unfiltered Coverage
| Number of Bases    | GOAT Genome Estimation (assembly span) | Calculated Coverage  |
|--------------------|----------------------------------------|----------------------|
| 128,506,025,356.00 |                            575,000,000 |          223x |

Genome coverage refers to the number of times a particular sequence is accurately mapped to a genome. In the table above, we used data from the [GoaT](https://goat.genomehubs.org/search?query=tax_name%2891209%29&result=taxon&includeEstimates=true&taxonomy=ncbi#tax_name(91209%5BJuglans%20ailantifolia%5D)) (Genomes on a Tree) platform, which leverages ancestral information of species to predict genome size. By combining this predicted genome size with the total number of bases from our raw reads, we could estimate the level of coverage we achieved.

## 02. Contaminant Removal

### Centrifuge
After analyzing the raw reads from Nanopore, our next step was to remove any potential contaminants from the dataset. We accomplished this using Centrifuge, which identified contaminants by comparing our sequences against a curated database of common fungal, bacterial, archaeal, and viral species. The database (f+b+a+v) revealed that 4.47% of the raw reads contained contaminants. These sequences were subsequently filtered out, ensuring a clean dataset before we proceeded with genome assembly. Contaminant sequences longer than 50 base pairs, which matched entries in the database, were identified and removed using grep and awk.

- **Full Script:** [Centrifuge.sh](Assembly Scripts/02_Quality_Control/Centrifuge.sh)

### Nanoplot

Nanoplot after Centrifuge:

| NanoPlot            | Raw Reads          |
|---------------------|--------------------|
| Mean read length    | 11,034.9           |
| Mean read quality   | 18.7               |
| Median read length  | 8,492.0            |
| Median read quality | 19.2               |
| Number of reads     | 11,124,549.0       |
| Read length N50     | 18,972.0           |
| STDEV read length   | 10,716.2           |
| Total bases         | 122,757,815,696.0  |

<img src="/uploads/f638dc8e67b32a6c1a0be1eb0bbdc889/LengthvsQualityScatterPlot_dot.png" alt="Alt text" width="500" height="500" />

After rerunning NanoPlot after removing potential contaminants, we saw that roughly 350,000 reads, or around 6 billion bases in total, were removed through filtration. This resulted in small decreases in the mean and median read lengths.

<img src="/uploads/a46a9eda7b54b522f129a439bb2b1ac4/652d6d2132452556fe37db39_1000x1000.png" alt="Alt text" width="600" height="400" />

> Description: This Sankey Visualization was generated with the use of Pavian. Graph shows the contaminants found within our sample.

**Top 3 Contaminants**
* *Escherichia coli*
* *Cutibacterium acnes*
* *Bacillus cereus* group

### Genome Size Estimation

We estimated the genome size using the kmerfreq and GCE programs based on our unfiltered and filtered reads. This approach used a Kmer-based method to calculate the genome size from our data.

- **Full_Script:** [Genome_Size_Estimation.sh](Assembly Scripts/02_Quality_Control/Genome_Size_Estimation.sh)

| Genome Size Estimation | Unfiltered | Filtered |
| ---------------------- | ---------- | -------- |
| Homozygous GCE         | 597 Mb     | 572 Mb   |
| Heterozygous GCE       | 597 Mb     | 572 Mb   |

Following the kmerfreq and Heterozygous GCE run on our data, we calculate the estimated genome size for our unfiltered reads to be 597 Mb. Post removal of the contaminants, the genome size estimation for our filtered reads is 572 Mb.

### Filtered Coverage
| Number of Bases    | GOAT Genome Estimation (assembly span) | Calculated Coverage  |
|--------------------|----------------------------------------|----------------------|
| 122,757,815,696.00 |                            575,000,000 |          213x |



As mentioned above, genome coverage refers to the number of times sequences were accurately mapped to an assembly. For the table above, we again used the GoaT (Genomes on a Tree) and the filtered reads to determine the estimated coverage.


**Coverage Map** 
![00001b](/uploads/cbb9a59e9da682fe932f38f9f9a072fa/00001b.png)

The coverage map above confirms our calculations, showing approximately 200x coverage. This analysis was performed using kmer data from the filtered reads. Such high coverage is highly favorable and suggests promising outcomes for the assembly process.


## 03. Assembling The Genome
### Flye
Flye is a de novo assembler for single molecule reads. By giving Flye our filtered reads produced by centrifuge, an assembly is created. 

- **Full Script:** [flye.sh](Assembly Scripts/03_Assembly/flye.sh)

Quality control was done after each Flye assembly run. Data collected from these runs using Quast and BUSCO are outlined under the 07. Assembly Statistics section. 

### Canu
Another de nevo assembler used was the Canu assembler. Canu has three major steps: 1.) Read Correction 2.) Read Trimming 3.) Contig Assembly. 

- **Full Script:** [canu.sh](Assembly Scripts/03_Assembly/canu.sh)

Quality control was done after each Canu assembly run. Data collected from these runs using Quast and BUSCO are outlined under the 07. Assembly Statistics section.

### Hifiasm
The last assembler tested using our raw reads was Hifiasm.  Hifiasm is a fast haplotype-resolved de novo assembler. 

- **Full Script:** [hifiasm.sh](Assembly Scripts/03_Assembly/hifiasm.sh)

Quality control was done after each Hifiasm assembly run. Data collected from these runs using Quast and BUSCO are outlined under the 07. Assembly Statistics section. 

> ⚠️ **Important Notice:** Results from the Hifiasm run were insufficient and have been discarded.


## 04. Error Correction With Medaka
### Medaka
Medaka was used to polish the Flye-generated assembly by aligning the long reads back to the assembly for error correction by consensus. This method involves choosing the most representative sequences and fixing the variants along the assembly. The Canu-generated assembly was not processed for error correction outside of the innate polishing. Quality control was performed on the assembly after error correction to measure the effectiveness of Medaka polishing.

- **Full Script:** [Medaka.sh](Assembly Scripts/05_Error_Correction/Medaka.sh)

## 05. Merqury
### Merqury Results
Merqury was used for quality control to estimate the base-level correctness and completeness of each assembly using the long reads as input. This tool is used alongside Meryl, a genomic k-mer counter, to compare k-mers between the assemblies and reads. QV quality scores as well as K-mer spectrum plot visualizations were generated for each assembly to evaluate results.

- **Meryl_Full_Script:** [meryl.sh](Assembly Scripts/04_Initial_Assembly_Evaluation/meryl.sh)

- **Merqury_Full_Script:** [merqury.sh](Assembly Scripts/04_Initial_Assembly_Evaluation/merqury.sh)

**Flye Plot**

<img src="/uploads/5e2b2aea40f53c9bdf4fb8714c7aea76/image.png" alt="Alt text" width="500" height="400" />

**Canu 10kb Plot**

<img src="/uploads/20ab5a88a35ac216f9ead3749790b22c/image.png" alt="Alt text" width="500" height="400" />

## 06. Heterozygosity Resolution 
### Purge Haplotigs
Purge Haplotigs was used to resolve heterozygosity levels in the Flye and Canu assemblies by identifying syntenic contig pairs and removing haplotig repeats. Minimap2 was first used to align each assembly with the longs reads and construct a read-depth histogram, from which the cutoff points could be determined to remove duplication.

- **Canu_Purge_Full_Script:** [canu_purge.sh](Assembly Scripts/07_Purge_Haplotigs/canu_purge.sh)

**Canu Histogram**
<img src="/uploads/ad201a14f932ddac7d0ddb5c941c3755/canu_aligned.bam.histogram.png" alt="Alt text" width="700" height="500" />

- **Flye_Purge_Full_Script:** [flye_purge.sh](Assembly Scripts/07_Purge_Haplotigs/flye_purge.sh)

**Flye Histogram**
<img src="/uploads/376eb327af232eed9709891c2fc690de/flye_aligned.bam.histogram.png" alt="Alt text" width="700" height="500" />

## 07. Assembly Statistics

| Reads                              | High accuracy duplex |               |              |           |              |             |                                |                  |
| ---------------------------------- | -------------------- | ------------- | ------------ | --------- | ------------ | ----------- | ------------------------------ | ---------------- |
| Assembly Stage                     | Flye Raw             | Flye + Medaka | Flye + Purge | Canu 10kb | Canu + Purge | Hifiasm Raw | Ragtag Scaffold Level Assembly | Chromosome-level |
| BUSCO (Dataset: Embryophyta_odb10) |                      |               |              |           |              |             |                                |                  |
| Complete (Single & Duplicated)     | 99.00%               | 98.90%        | 99.00%       | 99.20%    | 98.90%       | 99.10%      | 98.90%                         | 98.90%           |
| Single                             | 91.30%               | 91.00%        | 91.40%       | 69.00%    | 90.80%       | 15.40%      | 91.40%                         | 91.40%           |
| Duplicated                         | 7.70%                | 7.90%         | 7.60%        | 30.20%    | 8.10%        | 83.70%      | 7.50%                          | 7.50%            |
| Fragmented                         | 0.60%                | 0.70%         | 0.60%        | 0.40%     | 0.50%        | 0.30%       | 0.50%                          | 0.50%            |
| Missing                            | 0.40%                | 0.40%         | 0.40%        | 0.40%     | 0.60%        | 0.60%       | 0.60%                          | 0.60%            |
| QUAST                              |                      |               |              |           |              |             |                                |                  |
| Number of contigs                  | 1278                 | 1278          | 897          | 3071      | 373          | 9439        | 44                             | 16               |
| Largest contig (bp)                | 8111626              | 8155698       | 8111626      | 7347796   | 7347796      | 3050016     | 54008184                       | 54008184         |
| Total length (bp)                  | 541553995            | 543998325     | 520541171    | 739659024 | 530130071    | 1305078109  | 530162971                      | 526265674        |
| GC content (%)                     | 36.51                | 36.38         | 36.47        | 36.57     | 36.49        | 36.44       | 36.49                          | 36.46            |
| N50 (bp)                           | 1280763              | 1289187       | 1386575      | 1630280   | 2385147      | 439729      | 34414504                       | 34414504         |
| L50                                | 114                  | 114           | 106          | 124       | 70           | 817         | 7                              | 7                |
| #Ns per 100kb                      | 0.7                  | 0.04          | 0.73         | 0         | 0            | 0           | 6.21                           | 6.25             |
| MERQURY                            |                      |               |              |           |              |             |                                |                  |
| QV score                           | 39.5059              | 31.6183       | 39.3734      | 40.9251   | 40.3699      | 36.5213     | 40.3699                        | 40.428           |


## 08. Scaffolding Preparation
### Deciding on an Assembly Run

Assembly runs with the most promising quality control runs were used to proceed with Scaffolding. For our project, the Canu 10K run showed the most promising results.

### Using the *Juglans mandshurica* Genome

The *Juglans mandshurica* is a tree species in the same family as *Juglans ailantifolia*. With this genome, scaffolders can use the *Juglans mandshurica* genome to piece together the *Juglans ailantifolia* genome. 

## 09. Scaffolding

The scaffolding program, RagTag, was used to scaffoled the *Juglans ailantifolia* genome from the *Juglans mandshurica* genome.

- **RagTag_Full_Script:** [ragtag.sh](Assembly Scripts/09_Scaffolding/ragtag.sh)

The RagTag presented the following results
| Assembly Stage                     | Ragtag Scaffold Level Assembly | Chromosome-level |
|------------------------------------|--------------------------------|------------------|
| BUSCO (Dataset: Embryophyta_odb10) |                                |                  |
| Complete (Single & Duplicated)     |                         98.90% |           98.90% |
| Single                             |                         91.40% |           91.80% |
| Duplicated                         |                          7.50% |            7.10% |
| Fragmented                         |                          0.50% |            0.40% |
| Missing                            |                          0.60% |            0.70% |
| QUAST                              |                                |                  |
| Number of contigs                  |                             44 |               16 |
| Largest contig (bp)                |                       54008184 |         51885142 |
| Total length (bp)                  |                      530162971 |        527030964 |
| GC content (%)                     |                          36.49 |            36.59 |
| N50 (bp)                           |                       34414504 |         33909832 |
| L50                                |                              7 |                7 |
| #Ns per 100kb                      |                           6.21 |             8.33 |
| MERQURY                            |                                |                  |
| QV score                           |                        40.3699 |          19.0615 |


The results shown above indicated that the *Juglans ailantifolia* genome assembled into 16 chromosomes. This was consistent with estimates from the GoaT platform and findings from other members of the *Juglandaceae* family, supporting the accuracy of our results. The assembly from the RagTag run was ready to be used used for genome annotation.

# Next Page
- **Next Page:** [Annotation](https://gitlab.com/PlantGenomicsLab/Japanese-walnut-assembly-and-annotation/-/blob/main/README_Annotation.md?ref_type=heads) 
