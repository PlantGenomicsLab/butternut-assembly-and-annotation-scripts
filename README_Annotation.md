# Japanese Walnut Genome Annotation 

[TOC]


# Annotation Process
### Overall Annotation Process
![EASEL](/uploads/797fc2f22996e0387542df2a34949f8b/EASEL.png)
> **EASEL Workflow:** The figure above shows a comprehensive workflow of EASEL's internal workings which are supported through Nextflow for automation. Imgae created by Cynthia Webster.

A fully assembled genome is only so useful. The next step is genome annotation which is the process of identifying the structural and functional elements within the genome. Structural annotation involves mapping genes on the genome and functional annotation involves identifying or predicting the function of the gene.
The following graphic shows all of the individuals programs that need to be run to annotate a genome. Or, one could simply use [EASEL](https://gitlab.com/PlantGenomicsLab/easel) to automate all of these steps.

## 01. Data Preparation
In order to map genes onto the assembled genome, we must first acquire the gene sequences by gathering RNA sequence reads. These give us the coding regions of genes. the NCBI holds a comprehensive collection of publicly available RNA sequence data submitted by researchers. Unfortunately, there was no RNA sequence data available for _Juglans ailantifolia_ at the time of this study. Instead, we used RNA sequences for _Juglans mandshurica_ and _Juglans cinerea_ which are closely related. The first step is to generate a text file of the Sequence Read Archive (SRA) codes from the NCBI database. Then the fastq files for each SRA could be downloaded using the following code:

<details>
<summary>Download RNA sequences from the NCBI </summary>

```
#!/bin/bash
#SBATCH --job-name=pull_reads
#SBATCH --mail-user=student.name@email.edu
#SBATCH --mail-type=ALL
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=70G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err


echo `hostname`        # print the name of the host running the job
date                   # print the date

#################################################################
# Download fastq files from SRA 
#################################################################

# load necessary modules
module load parallel/20180122
module load sratoolkit/2.11.3

TMPDIR=/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/annotation/01_reads/mandshurica
export TMPDIR

vdb-config --interactive

cat mandshurica_accession_list.txt | parallel -j 2 fasterq-dump

ls *fastq | parallel -j 12 gzip

```

</details> 


## 02. Quality Control of RNA Reads
Now that we have downloaded the RNA sequences the next step is to clean up and preprocess them. This is done with fastp which filters out bad reads, trims the head and tail of the sequences, cuts out adaptors, and most importantly, provides comprehensive quality control data from before and after the filtering.  
	
<details> 
<summary>Fastp script: </summary>

```
#!/bin/bash
#SBATCH --job-name=fastp_trimming_mandshurica
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 12
#SBATCH --mem=40G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=name@email.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`
date

#################################################################
# Trimming/QC of reads using fastp
#################################################################
module load fastp/0.23.2
module load parallel/20180122

# set input/output directory variables
INDIR=/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/annotation/01_reads/mandshurica
REPORTDIR=fastp_reports
mkdir -p $REPORTDIR
TRIMDIR=trimmed_sequences
mkdir -p $TRIMDIR

ACCLIST=/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/annotation/01_reads/mandshurica/mandshurica_accession_list.txt

# run fastp in parallel, 4 samples at a time
cat $ACCLIST | parallel -j 4 \
fastp \
        --in1 $INDIR/{}_1.fastq.gz \
        --in2 $INDIR/{}_2.fastq.gz \
        --out1 $TRIMDIR/{}_trim_1.fastq.gz \
        --out2 $TRIMDIR/{}_trim_2.fastq.gz \
        --json $REPORTDIR/{}_fastp.json \
        --html $REPORTDIR/{}_fastp.html

```

</details>

|        | Before Filtering |        | After Filtering |   | Difference (Before-After) |   |
| ------ | ---------------- | ------ | --------------- | - | ------------------------- | - |
|        |Total Reads(Mb)   |Total Bases(Gb)|Total Reads(Mb)|Total Bases(Gb)|Total Reads(Mb)|Total Bases(Gb)| 
| SRR14692617 | 54.81 | 8.22 | 54.60 | 8.18 | 0.21 | 0.04 |
| SRR14692626 | 48.84 | 7.33 | 48.65 | 7.22 | 0.19 | 0.11 |
| SRR14792884 | 54.00 | 8.10 | 53.74 | 7.95 | 0.26 | 0.15 |
| SRR18352962 |	40.68 | 6.10 | 40.41 | 6.06 | 0.27 | 0.05 |
| SRR14692620 |	60.06 | 9.01 | 59.67 | 8.83 | 0.39 | 0.18 |
| SRR14792878 |	68.11 | 10.22 | 67.75 | 9.99 | 0.36 | 0.23 |
| SRR18063676 |	48.47 | 7.27 |	48.36 | 7.21 | 0.11 | 0.06 |
| SRR18352965 |	44.79 | 6.72 |	44.47 | 6.66 | 0.32 | 0.06 |
| SRR14692623 |	65.16 | 9.77 |	64.75 | 9.56 | 0.41 | 0.21 |
| SRR6134194 | 42.41 | 4.241 | 42.41 | 4.237 | 0 | 0.004 |
| SRR6134195 | 42.99 | 4.299 | 42.99 | 4.295 | 0 | 0.004 |
| SRR6434975 | 48.35 | 6.63 | 31.11 | 4.49 | 17.24 | 2.14 |

## 03. Read Alignment To Reference Genome and Indexing
Our trimmed RNA sequences are now ready to be mapped onto the assembled genome. We do this with the program HISAT2. We have also created a Nextflow pipeline that automates all the steps of HISAT2 by running one script. Thus it aligns the RNA sequences to the assembled genome, indexes the genome, and provides data on the mapping rate of each RNA fastq file.
The Nextflow pipeline includes five scripts for setup and one final one for running the pipeline.

<details><summary>alignments.nf:</summary>

```
process hisat2Index {
	publishDir "$params.outdir/index",  mode: 'copy'
	label 'hisat'

    	input:
    	path(genome)

    	output:
    	path "hisat2.index" , emit: hisat2index
    

    	script:
    	"""
    	hisat2-build -f ${genome} hisat2
	    mkdir hisat2.index
	    mv *.ht2 hisat2.index
    	"""
}


process hisat2Align {
    publishDir "$params.outdir/mapping_rate",  mode: 'copy', pattern: "*.txt"
	tag { id }
	label 'hisat'	

    	input:
    	tuple val(id), path(trimmed)
		path(index)

    	output:
    	tuple val(id), path("*.sam"), emit: sam
        path("*_mapping.txt"), emit: alignment_rates
    

    	script:
    	""" 
        hisat2 -q -x ${index}/hisat2 -1 ${trimmed[0]} -2 ${trimmed[1]} -S ${id}.sam --summary-file ${id}_mapping.txt -p ${task.cpus}
    	"""
}

process samtools {
    publishDir "$params.outdir/bam",  mode: 'copy', pattern: '*.bam'
	tag { id }	
	label 'hisat'

    	input:
    	tuple val(id), path(sam)

    	output:
		tuple val(id), path("*.bam"), emit: bam
		
    	script:
    	""" 
        samtools view -b -@ 16 ${sam} | samtools sort -o sorted_${id}.bam -@ ${task.cpus}
		
    	"""
}
```

</details>

<details><summary>task_alignments.nf</summary>

```
include { hisat2Index; hisat2Align; samtools } from '../modules/alignments.nf'

workflow hisat2 {

    take:
    genome
    reads
	
    main:
    hisat2Index		    ( genome )
    read_pairs_ch = Channel.fromFilePairs(reads)
    hisat2Align		    ( read_pairs_ch, hisat2Index.out.hisat2index )
    samtools		    ( hisat2Align.out.sam ) 
}
```

</details>

<details><summary>main_workflow.nf</summary>

```
include { hisat2 } from '../subworkflows/tasks_alignments.nf'

workflow main_workflow {
	
	hisat2 				( params.genome, params.reads ) 

}
```

</details>

<details><summary>main.nf</summary>

```
def helpMessage() {
	log.info"""
 	
	Usage:
	nextflow main.nf --genome /path/to/genome.fa --outdir /path/to/outdir --reads /path/to/reads [...] 
	
	Required arguments:
		--genome				Path to masked genome (*.fa/*.fasta)
		--reads					Path to SRA accession list

	Optional arguments:
		--outdir				Path to the output directory (default: easel_results)
		-resume					Resume script from last step 
	
    """.stripIndent()
}

params.help = false
if (params.help){
    helpMessage()
    exit 0
}

nextflow.enable.dsl=2

include { main_workflow as MAIN } from './workflows/main_workflow.nf'

workflow {

    MAIN()

    }
```

</details>

<details><summary>nextflow.config</summary>

```
process {
/*
generic process parameters
*/
    executor = 'slurm'
    clusterOptions = '--qos=general'
    queue = 'general'
    memory = '20G' 
    cpus = 4 

/*
named process requirements
*/    
    withLabel: hisat {
        module = '/isg/shared/modulefiles/hisat2/2.2.1:/isg/shared/modulefiles/samtools/1.16.1'
        executor = 'slurm'
        clusterOptions = '--qos=general'
        memory = '30G' 
        cpus = '8'
        queue = 'general'
    }
}

params {
    outdir                   = './hisat2_results'
    genome                   = ''
    reads                    = '/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/annotation/02_quality_control/Selected_trimmed_SRA/*trim_{1,2}.fastq.gz'  
}
```

</details>

<details><summary>nextflow.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=nextflow_HiSat2
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G
#SBATCH --mail-user=student.name@uconn.edu

module load nextflow

nextflow run main.nf --outdir /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/annotation/03_alignments --genome /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/10_evaluation/rename/renamed_chrom.fasta

```

</details>

| _Juglans cinerea_ | Mapping Rate |
| ------ | ------ |
| SRR6134194 | 86.70% |
| SRR6134195 | 87.54% |
| SRR6434975 | 40.62% |

| _Juglans mandshurica_ | Mapping Rate |
| ------ | ------ |
| SRR14692617 | 92.86%
| SRR14692626 | 93.15%
| SRR14792884 | 87.79%
| SRR18352962 | 93.10%
| SRR14692620 | 93.66%
| SRR14792878 | 93.88%
| SRR18063676 | 95.48%
| SRR18352965 | 89.65%
| SRR14692623 | 93.50%

The RNA sequence from SRR6434975 was dropped from consideration after HISAT2 for being below the 85% cutoff for mapping rates.

## 04. EASEL
The EASEL nextflow pipeline was used to automate several steps of the annotation process. This includes Augustus, ENTAP, Eggnog, as well as machine learning algorithms to create the structural and the functional annotations of a genome. This program takes in a softmasked genome and RNA sequences and produces filtered, unfiltered, primary isoform, and longest isoform annotation files as well as evaluation metrics for each one. The inputs are put into a params.yaml file and then the program is run by executing the nextflow.sh file.

<details><summary>nextflow.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=EASELv_1.4
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G
#SBATCH --mail-type=END
#SBATCH --mail-user=student.name@uconn.edu

echo 'hostname'

#If Nextflow and Singularity are not available as modules, be sure to follow dependency instructions
module load nextflow
module load singularity

#Singularity defaults to a scratch directory, I choose to redirect this into a new location with more storage
SINGULARITY_TMPDIR=$PWD/tmp
export SINGULARITY_TMPDIR

#The `xanadu` profile is specific to the UConn HPC, other available profiles can be found here: https://github.com/nf-core/configs
nextflow run -hub gitlab PlantGenomicsLab/easel-benchmarking-v2-nf -profile singularity,xanadu -params-file params.yaml
```

</details>

<details><summary>params.yaml</summary>

```
outdir    : "j_ailantifolia"
genome: "/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/annotation/05_EASEL/EASEL_Test_1/renamed_chrom.masked.fasta"
bam : "/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/annotation/03_alignments/bam/*.bam"
busco_lineage   : "embryophyta"
order : "eudicotyledons"
prefix     : "ailantifolia"
reference_db : "/isg/shared/databases/Diamond/RefSeq/plant.protein.faa.208.dmnd"
taxon : "juglans"
singularity_cache_dir: "/isg/shared/databases/nfx_singularity_cache"
training_set : 'plant'
```

</details>

### Final Summary

|   | Unfiltered | Filtered | Filtered (Primary Isoform) | Filtered (Longest Isoform) |
|----|----|----|----|----|
| Total number of genes | 57,883 | 26,213 | 26,213 | 26,213 |
| Total number of transcripts | 145,680 | 60,648 | 26,213 | 26,213 |
| EnTAP alignment rate | .79 | .95 | .97 | .95 |
| Mono-exonic/multi-exonic rate | .57 | .24 | .27 | .26 |
| BUSCO (embryophyta_odb10) |  |  |  |  |
| Complete | 99.6 | 99.6 | 93.5 | 97.7 |
| Single-Copy | 26.6 | 35.8 | 87.2 | 90.8 |
| Duplicated | 73.0 | 63.8 | 6.3 | 6.9 |
| Fragmented | 0.1 | 0.1 | 2.0 | 2.0 |
| Missing | 0.3 | 0.3 | 4.5 | 2.0 |

# Next Page
- **Next Page:** [Comparative Genomics](https://gitlab.com/PlantGenomicsLab/Japanese-walnut-assembly-and-annotation/-/blob/main/README_Comparative_Genomics.md?ref_type=heads) 