#!/bin/bash
#SBATCH --job-name=medaka.flye.walnut
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 32
#SBATCH --mem=180G
#SBATCH --partition=xeon
#SBATCH --qos=general                          
#SBATCH --mail-type=ALL                      
#SBATCH --mail-user=
#SBATCH -o %x_%A.out                         
#SBATCH -e %x_%A.err                         

module load medaka/1.7.1
module unload tabix/0.2.6

medaka_consensus -t 32 -i /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/02_quality_control/Centrifuge/filtered_Juglans_ailanthifolia.fasta -d /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/03_assembly/FlyeAnthony/flye-output/assembly.fasta -o medaka_flye_output

