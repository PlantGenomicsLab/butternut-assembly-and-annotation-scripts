#!/bin/sh
#SBATCH --job-name=fastq_to_fasta
#SBATCH -N 1
#SBATCH -n 1 
#SBATCH -c 1                         
#SBATCH --partition=general        
#SBATCH --qos=general                
#SBATCH --mail-type=END             
#SBATCH --mem=10G                   
#SBATCH --mail-user=               
#SBATCH -o %x_%j.out              
#SBATCH -e %x_%j.err

module load seqtk

seqtk seq -a Juglans_ailanthifolia.fastq.gz > Juglans_ailanthifolia.fasta
