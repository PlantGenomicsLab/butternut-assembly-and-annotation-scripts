#!/bin/bash
#SBATCH --job-name=pass
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 10
#SBATCH --mem=80G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "HOSTNAME: `hostname`"
echo "Start Time: `date`"


module load NanoPlot/1.33.0

NanoPlot --fastq /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/01_raw_reads/Juglans_ailanthifolia.fastq.gz  --loglength --verbose -o summary-fastqpass -t 10 -p summary-fastqpass 


# Generate a summary plot of the raw reads with log-transformed data
NanoPlot --fasta /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/01_raw_reads/Juglans_ailanthifolia.fasta -o summary-txt -t 10


echo "End Time: `date`"
date
module list