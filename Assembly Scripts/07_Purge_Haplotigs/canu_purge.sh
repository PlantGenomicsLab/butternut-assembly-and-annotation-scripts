!/bin/bash
#BATCH --job-name=purge_haplotigs
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=200G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

#module load minimap2/2.15
#module load samtools/1.9

ref="/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/03_assembly/Canu/Canu_Assembly_10kb/canu.contigs.fasta"
read_file="/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/02_quality_control/Centrifuge/filtered_Juglans_ailanthifolia.fasta"

#minimap2 -t 16 -ax map-ont ${ref} ${read_file} \
       #| samtools view -hF 256 - \
       #| samtools sort -@ 16 -m 1G -o canu_aligned.bam -T canu_tmp.ali

#module unload minimap2/2.15
#module unload samtools/1.9

##########################################
##      purge haplotigs                 ##
##########################################
module load purge_haplotigs/1.1.2
module load R/4.2.2
## STEP-1: you just created a histogram; look at it and find high, med, and low cutoffs for next step :)
#purge_haplotigs readhist -b canu_aligned.bam -g ${ref} -t 16 -d 300


#next steps for purge! run sequentially - first step's output is used in second step, etc.

## Step 2: use the cutoff values from histogram 
#purge_haplotigs  contigcov -i canu_aligned.bam.gencov -l 40 -m 160 -h 288


## Step 3: final step that purges seqs based on your cutoffs 
purge_haplotigs purge -b canu_aligned.bam -g ${ref} -c coverage_stats.csv -d