#!/bin/bash
#BATCH --job-name=purge_haplotigs
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 12
#SBATCH --mem=150G
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

#module load minimap2/2.15
#module load samtools/1.9

ref="/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/03_assembly/FlyeAnthony/flye-output/assembly.fasta"
read_file="/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/02_quality_control/Centrifuge/filtered_Juglans_ailanthifolia.fasta"

#minimap2 -t 16 -ax map-ont ${ref} ${read_file} \
#       | samtools view -hF 256 - \
#       | samtools sort -@ 16 -m 1G -o flye_aligned.bam -T flye_tmp.ali

module unload minimap2/2.15
module unload samtools/1.9

##########################################
##      purge haplotigs                 ##
##########################################
module load purge_haplotigs/1.1.2
module load R/4.2.2
## STEP-1
#purge_haplotigs readhist -b flye_aligned.bam -g ${ref} -t 12 -d 300

#next steps for purge! run sequentially - first step's output is used in second step, etc.

## Step 2: use the cutoff values from histogram
#purge_haplotigs  contigcov -i flye_aligned.bam.gencov -l 56 -m 152 -h 288


## Step 3: final step that purges seqs based on your cutoffs
purge_haplotigs purge -b flye_aligned.bam -g ${ref} -c coverage_stats.csv -d