#!/bin/bash
#SBATCH --job-name=length
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=stefan.wnuk@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

module load bioawk/1.0

echo "length after scaffolding" > chromosome_length.csv

bioawk -c fastx '{print length($seq)}' /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/10_evaluation/length/chromosome_contigs.fasta >> chromosome_length.csv

