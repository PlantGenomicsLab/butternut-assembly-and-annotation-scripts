#!/bin/bash
#SBATCH --job-name=length
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=stefan.wnuk@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

module load bioawk/1.0

bioawk -c fastx 'length($seq) > 10000000{ print ">"$name; print $seq }' /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/09_scaffolding/ragtag_output/ragtag.scaffold.fasta >> chromosome_contigs.fasta
