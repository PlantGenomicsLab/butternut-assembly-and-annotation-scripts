#!/bin/bash
#SBATCH -J ragtag
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 32
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mem=100G
#SBATCH --mail-type=END
#SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`
module load RagTag/2.1.0

# scaffold a query assembly
ragtag.py scaffold /core/projects/EBP/conservation/can_butternut/juglans_reference/GCA_022457165.1_NFU_Jman_1.0_genomic.fna /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/07_purge_haplotigs/Canu_Purge/curated.fasta -f 1000
