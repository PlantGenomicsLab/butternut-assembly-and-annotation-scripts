#!/bin/bash
#SBATCH --job-name=smudge               
#SBATCH -N 1            
#SBATCH -n 1            
#SBATCH -c 10           
#SBATCH --partition=general      
#SBATCH --qos=general            
#SBATCH --mail-type=all          
#SBATCH --mem=200G               
#SBATCH --mail-user=
#SBATCH -o %x_%j.out                       
#SBATCH -e %x_%j.err       

module load jellyfish/2.2.6

# count 17-mers with jellyfish
#jellyfish count -t 32 -C -m 17 -s 100000000 -o 17mer_out --min-qual-char=? /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/02_quality_control/Centrifuge/filtered_Juglans_ailanthifolia.fasta

# generate histogram
#jellyfish histo -o jap_walnut.histo 17mer_out

# find cutoffs of plot
#L=$(smudgeplot.py cutoff jap_walnut.histo L)
#U=$(smudgeplot.py cutoff jap_walnut.histo U)
#echo $L $U

jellyfish dump -c -L 75 -U 1800 17mer_out | smudgeplot.py hetkmers -o kmer
#smudgeplot.py plot kmer_coverages.tsv -o j_walnut_smudgeplot
