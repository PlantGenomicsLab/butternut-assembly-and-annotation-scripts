#!/bin/bash
#SBATCH --job-name=test_filtered_genome_size
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

# calculate k-mer frequency using kmerfreq
 #/core/projects/EBP/conservation/software/kmerfreq/kmerfreq -k 17 -t 10 read_files.lib

echo "complete calculated k-mer frequency using kmerfreq"

# extract k-mer individual number
 #less read_files.lib.kmer.freq.stat | grep "#Kmer indivdual number"
 #less read_files.lib.kmer.freq.stat | perl -ne 'next if(/^#/ || /^\s/); print; ' | awk '{print $1"\t"$2}' > read_files.lib.kmer.freq.stat.2colum 

# run GCE in homozygous mode
 /core/projects/EBP/conservation/software/GCE/gce-1.0.2/gce -g 122579822915 -f read_files.lib.kmer.freq.stat.2colum >gce.table 2>gce.log

# run GCE in heterozygous mode 
 /core/projects/EBP/conservation/software/GCE/gce-1.0.2/gce -g 122579822915 -f read_files.lib.kmer.freq.stat.2colum  >gce2.table 2>gce2.log
