#!/bin/bash
#SBATCH --job-name=canu_10kb
#SBATCH -N 1               
#SBATCH -n 1               
#SBATCH -c 15           
#SBATCH --partition=general
#SBATCH --qos=general      
#SBATCH --mail-type=ALL    
#SBATCH --mem=35G          
#SBATCH --mail-user=
#SBATCH -o canu%x_%j.out         
#SBATCH -e canu%x_%j.err         

module load gnuplot/5.2.2
module load canu/2.2

echo "begin"

canu -p canu -d Canu_Assembly_10kb \
	genomeSize=600m \
        -minReadLength=10000 \
        -gridOptions="--partition=general --qos=general" canuIteration=1 \
        -nanopore /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/02_quality_control/Centrifuge/filtered_Juglans_ailanthifolia.fasta  

echo "complete" 
