#!/bin/bash
#SBATCH --job-name=flye
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 32
#SBATCH --mem=300G
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mail-user=
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err


module load flye/2.9.1

flye --nano-hq /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/02_quality_control/Centrifuge/filtered_Juglans_ailanthifolia.fasta --no-alt-contigs --threads 32 --out-dir flye-output --scaffold --asm-coverage 60 --genome-size 572M
