#!/bin/bash
#BAqTCH --job-name=hifiasm
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 32
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mem=250G
#SBATCH -o myscript_%j.out
#SBATCH -e myscript_%j.err


hifiasm=/core/projects/EBP/conservation/software/hifiasm/hifiasm
reads=/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/02_quality_control/Centrifuge/Juglans_ailantifolia_pass_filtered.fastq.gz


$hifiasm -o japanese_walnut.asm -t 32 $reads
