#!/bin/bash
#SBATCH --job-name=hifiasm_busco
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 15
#SBATCH --mem=20G
#SBATCH --partition=xeon #pay attention!!! we are using xeon, not general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err



module load busco/5.4.5

busco -i /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/03_assembly/hifiasm/japanese_walnut.asm.bp.p_ctg.fa \
        -o hifiasm_busco_output -l embryophyta_odb10 -m genome -c 15