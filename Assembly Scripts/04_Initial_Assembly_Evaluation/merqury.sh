#!/bin/bash
#SBATCH --job-name=merqury
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 32
#SBATCH --mem=128G
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mail-user=
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err

# Load Singularity
module load singularity

# Define input and output file names
output_filtered_kmer_db="/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/04_inicial_assembly_evaluation/meryl_db/kmer_db.filtered.meryl"
output_hifiasm="/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/03_assembly/hifiasm/japanese_walnut.asm.bp.p_ctg.fa"
output_prefix="j_walnut_hifiasm"

# Execute merqury.sh
singularity exec /isg/shared/databases/nfx_singularity_cache/merqury.sif merqury.sh \
    $output_filtered_kmer_db \
    $output_hifiasm \
    $output_prefix
