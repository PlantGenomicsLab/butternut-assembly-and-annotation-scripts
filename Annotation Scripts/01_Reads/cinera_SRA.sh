#!/bin/bash
#SBATCH --job-name=pull_reads
#SBATCH --mail-user=Cristopher.Guzman@Uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=70G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err


echo `hostname`        # print the name of the host running the job
date                   # print the date

#################################################################
# Download fastq files from SRA 
#################################################################

# load necessary modules
module load parallel/20180122
module load sratoolkit/2.11.3

TMPDIR=/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/annotation/01_reads/cinerea
export TMPDIR

vdb-config --interactive

cat cinerea_accession_list.txt | parallel -j 2 fasterq-dump

ls *fastq | parallel -j 12 gzip