#!/bin/bash
#SBATCH --job-name=repeat_masker
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=70G
#SBATCH -o %x_%j.out                
#SBATCH -e %x_%j.err

module load singularity/3.9.2     

singularity exec /core/projects/EBP/software/TEtools/dfam-tetools-latest.sif RepeatMasker -dir repeatmasker_out -pa 16 -lib RM_*/consensi.fa.classified -gff -a -noisy -xsmall /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/10_evaluation/rename/renamed_chrom.fasta


