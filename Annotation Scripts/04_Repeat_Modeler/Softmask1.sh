#!/bin/bash
#SBATCH --job-name=repeat_db
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=70G
#SBATCH -o %x_%j.out                
#SBATCH -e %x_%j.err
#SBATCH --mail-type=ALL                  
#SBATCH --mail-user=amanda.mueller@uconn.edu                     

module load  singularity/biosim-3.10.0

singularity exec /core/projects/EBP/software/TEtools/dfam-tetools-latest.sif BuildDatabase -name "rm_database" /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/10_evaluation/rename/renamed_chrom.fasta
