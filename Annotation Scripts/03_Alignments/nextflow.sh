#!/bin/bash
#SBATCH --job-name=nextflow_HiSat2
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G
#SBATCH --mail-user=stefan.wnuk@uconn.edu

module load nextflow

#replace genome and outdir
nextflow run main.nf --outdir /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/annotation/03_alignments --genome /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/10_evaluation/rename/renamed_chrom.fasta
