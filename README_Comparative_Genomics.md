# Japanese Walnut Comparative Genomics

[TOC]

# Comparative Genomics

## 01. Methylation
Methylation refers to the biochemical process whereby a methyl group (CH3) is added to the DNA molecule, usually at cytosine bases that are next to guanine bases (CpG sites). This epigenetic modification does not change the underlying DNA sequence, but it can have a major impact on gene expression, acting as a switch to turn genes on or off. Methylation patterns are mapped and investigated in genome assembly and analysis to comprehend their function in controlling genes as variations in these patterns are linked to diseases and developmental processes. Using software tools and visualization techniques, we examined the function of methylation in plant gene regulation in order to gain insights into the genomic landscape of *Juglans ailantifolia*.

### Basecalling and Remapping
Using a large-set of pod5 files, Dorado (version 0.4.2) was run for the basecalling process where raw sequencing signals were translated into nucleotide sequences with a focus on distinguishing methylated from unmethylated cytosines. Remapping the aligned sequences to a reference genome where Dorado identified methylated sites and quantified methylation percentage. 

<details><summary>Dorado script</summary>

```
#!/bin/bash
#SBATCH --job-name=dorado.sh
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=gpu
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=150G
#SBATCH --mail-user=keertana.chagari@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`

module load Dorado/0.5.0
#dorado download --model dna_r10.4.1_e8.2_400bps_sup@v4.3.0

dorado basecaller dna_r10.4.1_e8.2_400bps_sup@v4.3.0 \
        /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/dorado/pod5_files \
        --min-qscore 10 \
        --modified-bases 5mCG_5hmCG \
        --verbose \
        --device cuda:all \
        --reference /core/labs/Wegrzyn/juglans/hic/JAPANESE_WALNUT/samba/yahs/japanese_walnut_chr.fasta > japanesewalnut_methyl.bam
   
```

</details>

### Summarization
We used the modkit software (version 0.2.5-rc2) to process the methylation calls’ output and compile the methylation data. In tandem, .bed file in all sequence contexts (CHH, CHG, and CPG) were created. Samtools (version 1.16.1) and minimap2 (version 2.26) were also involved in the process.  

<details><summary>Modkit script</summary>

```
#!/bin/bash
#SBATCH --job-name=ont_modkit
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=keertana.chagari@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load samtools/1.16.1
module load minimap2/2.26
module load modkit/0.2.5-rc2

#sort bam file 
#samtools sort -o sorted_JWalnut_methyl.bam /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/dorado/dorado_kc/japanesewalnut_methyl.bam

#index bed file 
#samtools index -@ 12 sorted_JWalnut_methyl.bam -o sorted_JWalnut_methyl.bam.bai

modkit pileup -k --combine-strands --motif CG 0 --ignore h --edge-filter 12 --ref /core/labs/Wegrzyn/juglans/hic/JAPANESE_WALNUT/samba/yahs/japanese_walnut_chr.fasta sorted_JWalnut_methyl.bam ./pileup_cpg_try5.bed

modkit pileup -k --ignore h --motif CHG 0 --ref /core/labs/Wegrzyn/juglans/hic/JAPANESE_WALNUT/samba/yahs/japanese_walnut_chr.fasta sorted_JWalnut_methyl.bam ./pileup_chg_try6.bed

modkit pileup -k --ignore h --motif CHH 0 --ref /core/labs/Wegrzyn/juglans/hic/JAPANESE_WALNUT/samba/yahs/japanese_walnut_chr.fasta sorted_JWalnut_methyl.bam ./pileup_chh_try6.bed
```

</details>

### Visualization
Visualizing the methylation patterns of the gene body and varying repeat classes across the three methylation contexts was done through a series of steps involving the use of samtools (v.1.16.1), bedtools (v.2.29.0), and deeptools (v.3.5.0).

<details><summary>chromsizes.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=chromsizes
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=keertana.chagari@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load samtools/1.16.1

samtools faidx /core/labs/Wegrzyn/juglans/hic/JAPANESE_WALNUT/samba/yahs/japanese_walnut_chr.fasta 
cut -f1,2 /core/labs/Wegrzyn/juglans/hic/JAPANESE_WALNUT/samba/yahs/japanese_walnut_chr.fasta.fai > mychrom.sizes
```

</details>

<details><summary>makebw.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=jw_makebw
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 2
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=keertana.chagari@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load bedtools/2.29.0
bedGraphToBigWig_path="/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/bedGraphToBigWig"

#chh flag 
sed 's/ \+/\t/g' /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/ont_modkit/pileup_chh_try6.bed > test_chh_tab.bed 
awk -F'\t' '{print $1 "\t" $2 "\t" $3 "\t" $11}' test_chh_tab.bed > test_chh.bedgraph
LC_COLLATE=C sort -k1,1 -k2,2n test_chh.bedgraph > test_chh_sorted.bedgraph

bedtools merge -i test_chh_sorted.bedgraph -c 4 -o mean > test_chh_merged.bed
$bedGraphToBigWig_path test_chh_merged.bed /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes chh.bw 

#chg flag
sed 's/ \+/\t/g' /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/ont_modkit/pileup_chg_try6.bed > test_chg_tab.bed        
awk -F'\t' '{print $1 "\t" $2 "\t" $3 "\t" $11}' test_chg_tab.bed > test_chg.bedgraph
LC_COLLATE=C sort -k1,1 -k2,2n test_chg.bedgraph > test_chg_sorted.bedgraph

bedtools merge -i test_chg_sorted.bedgraph -c 4 -o mean > test_chg_merged.bed
$bedGraphToBigWig_path test_chg_merged.bed /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes chg.bw

#cpg flag 
sed 's/ \+/\t/g' /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/ont_modkit/pileup_cpg_try5.bed > cpg_tab.bed
awk -F'\t' '{print $1 "\t" $2 "\t" $3 "\t" $11}' cpg_tab.bed > cpg.bedgraph
LC_COLLATE=C sort -k1,1 -k2,2n cpg.bedgraph > cpg_sorted.bedgraph

bedtools merge -i cpg_sorted.bedgraph -c 4 -o mean > cpg_merged.bed
$bedGraphToBigWig_path cpg_merged.bed /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes cpg.bw
```

</details>


To visualize methylation across the gene body, use Rmsk2bed to convert EASEL gff file into .bed format and sort: 
<details><summary>genefile.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=annotation_genes
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=keertana.chagari@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

awk '$3 == "gene"' /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/annotation/05_EASEL/j_ailantifolia/final_predictions/ailantifolia_filtered.gff | awk -F'\t' 'BEGIN {OFS = FS} {print $1,$4,$5,$9,".",$7}' | sed 's/ID=//' > Jw_gene_annotations_gene.bed

#sort .bed file 
module load bedtools/2.29.0

bedtools sort -g /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes -i Jw_gene_annotations_gene.bed > Jw_gene_annotations_gene.sort.bed
```

</details>

<details><summary>deeptools.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=gb_jw
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=40G
#SBATCH--mail-user=keertana.chagari@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load deeptools/3.5.0

GENE_FILE="Jw_gene_annotations_gene.sort.bed"

computeMatrix scale-regions -S /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/chh.bw \
                               /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/chg.bw \
                               /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/cpg.bw \
        -R $GENE_FILE \
        --beforeRegionStartLength 5000 \
        --afterRegionStartLength 5000 \
        -o matrix.gz \
        --binSize 100 \
        -p 8

plotProfile -m matrix.gz \
        --outFileName gene_body_repeats.svg \
        --numPlotsPerRow 1 \
        --perGroup \
        --regionsLabel 'Gene Body (25,040)' \
        --startLabel "5'" \
        --endLabel "3'" \
        --yMin 0 \
        --yMax 100
```

</details>

To visualize methylation across different repeat families, use Rmsk2bed to convert the RepeatMasker .out file to .bed file format:
<details><summary>rmsk2bed.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=rmsk2bed_jap
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=keertana.chagari@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

awk 'BEGIN{OFS="\t"}{if(NR>3) {if($9=="C"){strand="-"}else{strand="+"};print $5,$6-1,$7,$10,$11".",strand}}' /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/annotation/TEtrimmer/ACTUALLY_FINAL_FINAL_WOW/j_walnut/03_tetrimmer/tetrimmer_repeatmasker_out/update/final.out > jap_wal.final.bed
```

</details>

<details><summary>sort_bed.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=repeats
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=keertana.chagari@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

#sort .bed file 
module load bedtools/2.29.0

bedtools sort -g /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes -i jap_wal.final.bed > jap_wal.final.sort.bed
```

</details>
 
<details><summary>grep.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=grep
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=80G
#SBATCH --mail-user=keertana.chagari@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

grep -i 'LTR/Gypsy' jap_wal.final.sort.bed > gypsy_repeats.bed
grep -i 'LTR/Copia' jap_wal.final.sort.bed > copia_repeats.bed
grep -i 'DNA' jap_wal.final.sort.bed > dna_repeats.bed
grep -i 'LINE' jap_wal.final.sort.bed > line_repeats.bed
grep -i 'PLE' jap_wal.final.sort.bed > ple_repeats.bed
grep -i 'SINE' jap_wal.final.sort.bed > sine_repeats.bed
grep -i 'LTR' jap_wal.final.sort.bed > ltr_repeats.bed
```

</details>

<details><summary>copia_deeptools.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=copia_jw
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 6
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=keertana.chagari@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load deeptools/3.5.0

REPEAT_CLASS="LTR/Copia"

#Compute matrix
computeMatrix scale-regions -S /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/chh.bw \
                               /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/chg.bw \
                               /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/cpg.bw \
        -R copia_repeats.bed \
        --beforeRegionStartLength 5000 \
        --afterRegionStartLength 5000 \
        -o matrix.gz \
        --binSize 100 \
        -p 6

#Plot profile
plotProfile -m matrix.gz \
        --outFileName copia_repeats_profile.png \
        --numPlotsPerRow 1 \
        --perGroup \
        --regionsLabel 'LTR/Copia (43,041)' \
        --startLabel "Start" \
        --endLabel "End" \
        --yMin 0 \
        --yMax 100
```

</details>

To go further and visualize methylation of repeat subfamilies, repeat the process by first extracting all repeat subfamilies from sorted RepeatMasker.out file:
<details><summary>grep.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=grep_copia_jap
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=80G
#SBATCH --mail-user=keertana.chagari@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

grep -i 'LTR/Copia/Ale' jap_wal.final.sort.bed > copia_ale.bed
grep -i 'LTR/Copia/Bianca' jap_wal.final.sort.bed > copia_bianca.bed
grep -i 'LTR/Copia/Oryco' jap_wal.final.sort.bed > copia_oryco.bed
grep -i 'LTR/Copia/Sire' jap_wal.final.sort.bed > copia_sire.bed
grep -i 'LTR/Copia/Tork' jap_wal.final.sort.bed > copia_tork.bed
grep -i 'LTR/Copia/Ikeros' jap_wal.final.sort.bed > copia_ikeros.bed
grep -i 'LTR/Copia/Ivana' jap_wal.final.sort.bed > copia_ivana.bed
grep -i 'LTR/Copia/TAR' jap_wal.final.sort.bed > copia_tar.bed
```

</details>

<details><summary>ale_deeptools.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=ale_jap
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 6
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=keertana.chagari@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load deeptools/3.5.0

REPEAT_CLASS="COPIA/ALE"

#Compute matrix
computeMatrix scale-regions -S /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/chh.bw \
                               /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/chg.bw \
                               /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/cpg.bw \
        -R copia_ale.bed \
        --beforeRegionStartLength 5000 \
        --afterRegionStartLength 5000 \
        -o ale_matrix.gz \
        --binSize 100 \
        -p 6

#Plot profile
plotProfile -m ale_matrix.gz \
        --outFileName copia_ale_repeats_profile.png \
        --numPlotsPerRow 1 \
        --perGroup \
        --regionsLabel 'COPIA/ALE (12,394)' \
        --startLabel "Start" \
        --endLabel "End" \
        --yMin 0 \
        --yMax 100
```

</details>

To visualize the frequency of repeats relative to genes, make bigwig files of each repeat family to then use in another deeptools script:
<details><summary>makebw.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=copiajw_makebw
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 2
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=keertana.chagari@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load bedtools/2.29.0
bedGraphToBigWig_path="/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/bedGraphToBigWig"

sed 's/ \+/\t/g' /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/copia_plots/copia_ale.bed > test_ale.bed
bedtools genomecov -bg -i test_ale.bed -g /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes > test_ale.bedgraph
LC_COLLATE=C sort -k1,1 -k2,2n test_ale.bedgraph > test_ale_sorted.bedgraph

bedtools merge -i test_ale_sorted.bedgraph -c 3 -o mean > test_ale_merged.bed
$bedGraphToBigWig_path test_ale_merged.bed /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes ale.bw

sed 's/ \+/\t/g' /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/copia_plots/copia_bianca.bed > test_bianca.bed
bedtools genomecov -bg -i test_bianca.bed -g /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes > test_bianca.bedgraph
LC_COLLATE=C sort -k1,1 -k2,2n test_bianca.bedgraph > test_bianca_sorted.bedgraph

bedtools merge -i test_bianca_sorted.bedgraph -c 3 -o mean > test_bianca_merged.bed
$bedGraphToBigWig_path test_bianca_merged.bed /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes bianca.bw

sed 's/ \+/\t/g' /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/copia_plots/copia_oryco.bed > test_oryco.bed
bedtools genomecov -bg -i test_oryco.bed  -g /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes > test_oryco.bedgraph
LC_COLLATE=C sort -k1,1 -k2,2n test_oryco.bedgraph > test_oryco_sorted.bedgraph

bedtools merge -i test_oryco_sorted.bedgraph -c 3 -o mean > test_oryco_merged.bed
$bedGraphToBigWig_path test_oryco_merged.bed /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes oryco.bw

sed 's/ \+/\t/g' /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/copia_plots/copia_sire.bed > test_sire.bed
bedtools genomecov -bg -i test_sire.bed -g /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes > test_sire.bedgraph
LC_COLLATE=C sort -k1,1 -k2,2n test_sire.bedgraph > test_sire_sorted.bedgraph

bedtools merge -i test_sire_sorted.bedgraph -c 3 -o mean > test_sire_merged.bed
$bedGraphToBigWig_path test_sire_merged.bed /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes sire.bw

sed 's/ \+/\t/g' /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/copia_plots/copia_tork.bed > test_tork.bed
bedtools genomecov -bg -i test_tork.bed -g /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes > test_tork.bedgraph
LC_COLLATE=C sort -k1,1 -k2,2n test_tork.bedgraph > test_tork_sorted.bedgraph

bedtools merge -i test_tork_sorted.bedgraph -c 3 -o mean > test_tork_merged.bed
$bedGraphToBigWig_path test_tork_merged.bed /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes tork.bw

sed 's/ \+/\t/g' /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/copia_plots/copia_ikeros.bed > test_ikeros.bed
bedtools genomecov -bg -i test_ikeros.bed -g /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes > test_ikeros.bedgraph
LC_COLLATE=C sort -k1,1 -k2,2n test_ikeros.bedgraph > test_ikeros_sorted.bedgraph

bedtools merge -i test_ikeros_sorted.bedgraph -c 3 -o mean > test_ikeros_merged.bed
$bedGraphToBigWig_path test_ikeros_merged.bed /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes ikeros.bw

sed 's/ \+/\t/g' /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/copia_plots/copia_ivana.bed > test_ivana.bed
bedtools genomecov -bg -i test_ivana.bed -g /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes > test_ivana.bedgraph
LC_COLLATE=C sort -k1,1 -k2,2n test_ivana.bedgraph > test_ivana_sorted.bedgraph

bedtools merge -i test_ivana_sorted.bedgraph -c 3 -o mean > test_ivana_merged.bed
$bedGraphToBigWig_path test_ivana_merged.bed /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes ivana.bw

sed 's/ \+/\t/g' /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/copia_plots/copia_tar.bed > test_tar.bed
bedtools genomecov -bg -i test_tar.bed -g /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes > test_tar.bedgraph
LC_COLLATE=C sort -k1,1 -k2,2n test_tar.bedgraph > test_tar_sorted.bedgraph

bedtools merge -i test_tar_sorted.bedgraph -c 3 -o mean > test_tar_merged.bed
$bedGraphToBigWig_path test_tar_merged.bed /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes tar.bw
```

</details>

<details><summary>repeatgenes_copia.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=test_repeatscopiagenes
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=80G
#SBATCH--mail-user=keertana.chagari@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load deeptools/3.5.0

cut -f1-3 Jw_gene_annotations_gene.sort.bed > Jw_gene_positions.bed

computeMatrix scale-regions -S ale.bw \
                               bianca.bw \
                               oryco.bw \
                               sire.bw \
                               tork.bw \
                               ikeros.bw \
                               ivana.bw \
                               tar.bw \
        --regionsFileName Jw_gene_positions.bed \
        --beforeRegionStartLength 5000 \
        --afterRegionStartLength 5000 \
        --outFileName allwalnut.gz \
        --missingDataAsZero \
        --binSize 100 \
        -p 8

plotProfile -m allwalnut.gz \
        --outFileName repeatsgenes_copia.svg \
        --numPlotsPerRow 1 \
        --perGroup \
        --regionsLabel 'Copia repeat frequency' \
        --startLabel "5'" \
        --endLabel "3'" \
        --yMin 0 \
        --yMax 500000
```

</details>

A plot that visualizes all repeat subfamilies together relative to genes follows similar steps but with a single bigwig file:
<details><summary>all_copia_makebw.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=all_copia_makebw
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 2
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=keertana.chagari@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load bedtools/2.29.0
bedGraphToBigWig_path="/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/bedGraphToBigWig"

sed 's/ \+/\t/g' /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/genes_from_annotation/repeats_graph/copia_repeats.bed > test_copia.bed
bedtools genomecov -bg -i test_copia.bed -g /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes > test_copia.bedgraph
LC_COLLATE=C sort -k1,1 -k2,2n test_copia.bedgraph > test_copia_sorted.bedgraph

bedtools merge -i test_copia_sorted.bedgraph -c 3 -o mean > test_copia_merged.bed
$bedGraphToBigWig_path test_copia_merged.bed /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/methylation/visuals/mychrom.sizes copia.bw
```

</details>

<details><summary>all_copia_frequency.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=wal_all_copiagenes
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=80G
#SBATCH--mail-user=keertana.chagari@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load deeptools/3.5.0

cut -f1-3 Jw_gene_annotations_gene.sort.bed > Jw_gene_positions.bed

computeMatrix scale-regions -S copia.bw \
        --regionsFileName Jw_gene_positions.bed \
        --beforeRegionStartLength 5000 \
        --afterRegionStartLength 5000 \
        --outFileName allwalnut.gz \
        --missingDataAsZero \
        --binSize 100 \
        -p 8

plotProfile -m allwalnut.gz \
        --outFileName all_copia.svg \
        --numPlotsPerRow 1 \
        --perGroup \
        --regionsLabel 'Copia repeat frequency' \
        --startLabel "5'" \
        --endLabel "3'" \
        --yMin 0 \
        --yMax 1500000
```

</details>


## 02. Gene Family Analysis
### Overview
Orthogroup construction was conducted on *J. ailantifolia* along with a total of 11 closely related members of the Juglandaceae family: *Alfaropsis roxburghiana, Carya illinoinensis, Cyclocarya paliurus, Juglans cinerea, Juglans hindsii, Juglans mandshurica, Juglans microcarpa x regia, Juglans nigra, Juglans regia, Pterocarya stenoptera,* and *Platycarya strobilacea*. The proteins were extracted from various databases including NCBI, CNCB, and CMB.BNU. If protein files were available then these were used directly, otherwise agat v1.2.0 was used to select the longest isoforms which were then used to create protein files. These protein files were evaluated for completeness with Busco v.5.4.5 and then run through Orthofinder v.2.5.4 to find orthologous gene sequences between the species.

Of the orthogroups returned by Orthofinder, the groups with at least 50 sequences, and in which more than 60% of the sequences were from a single species, were selected for further analysis. These protein sequences were then run through an hmmscan with hmmer v.3.3.2 to find which Pfam domains they contained. This was done to remove all of the orthogroups with transposons, viral proteins, and other extraneous, repeat sequences.

Following this removal, the edited protein files were again evaluated with Busco v.5.4.5 to ensure that highly conserved genes were not removed and then run through Orthofinder v.2.5.4 a second time to ensure that the sequences with a large amount of repeats were removed

Finally, the results from Orthofinder were passed through CAFE v.5.1.0 to identify expansion or contraction among gene families.

### Pipeline Scripts

<details><summary>Extracting Longest Isoforms</summary>

```
###converts transcript feature in 3rd column to mRNA for consistency### 
sed 's/transcript/mRNA/g' genomic.gff > mRNA.gff

###use AGAT to pull longest isoform###

module load singularity 

singularity exec /isg/shared/databases/nfx_singularity_cache/depot.galaxyproject.org-singularity-agat-1.2.0--pl5321hdfd78af_0.img agat_sp_keep_longest_isoform.pl -gff mRNA.gff -o longest_isoform.gff


###pull protein sequences from longest isoform GFF file###
###replace -f with proper genome assembly *.fna###

singularity exec /isg/shared/databases/nfx_singularity_cache/depot.galaxyproject.org-singularity-agat-1.2.0--pl5321hdfd78af_0.img
agat_sp_extract_sequences.pl -g longest_isoform.gff -f genome.fna -p -o proteins.pep
```

</details>

<details><summary>Orthofinder.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=OF_RefGenmomes
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=16G
#SBATCH --mail-user=first.last@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
echo -e "\nStart time:"
date

module load OrthoFinder/2.5.4

python $ORTHOFINDER -t 16 -f /path_to_directory_with_protein_files

echo -e "\nEnd time:"
date

```

</details>

<details><summary>Busco.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=busco
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=16G
#SBATCH --mail-user=first.last@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo -e "\nStart time:"
date

module load busco/5.4.5

busco -i protein_file.pep -o prefix_for_busco_outputs -l embryophyta_odb10 -m protein -c 16

echo -e "\nEnd time:"
date
```

</details>

<details><summary>pfam.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=pfam
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=40G
#SBATCH --array=[0-11]

module load hmmer/3.3.2
R1=($(ls -1 *.pep))
INT1=${R1[$SLURM_ARRAY_TASK_ID]}
OUT1=$(echo $INT1 | sed 's/.pep/.domtblout/')

hmmscan --cpu 4 --domtblout ${OUT1} /isg/shared/databases/Pfam/Pfam-A.hmm ${INT1}
```

</details>

<details><summary>filter.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=choose_repeats
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=1G
#SBATCH --mail-type=END
#SBATCH --mail-user=first.last@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

# Define the list of file pairs
file_pairs=(
    "a_roxburghiana"
    "c_illinoinensis"
    "c_paliurus"
    "j_ailantifolia"
    "j_cinerea"
    "j_hindsii"
    "j_mandshurica"
    "j_microcarpaXregia"
    "j_nigra"
    "j_regia"
    "p_stenoptera"
    "p_strobilacea"
)

# Define the list of keywords to search for
list_c=("transpos" "reverse transcriptase" "mule" "viral" "envelope" "dde sup" "integrase")

# Outer loop: iterate over each species in the file_pairs list
for species in "${file_pairs[@]}"; do
    # Create a new text file for each species
    touch "${species}_repeats.txt"
    
    # Inner loop: iterate over each string in species.txt
    while IFS= read -r string; do
        # Search for the string in species.domtblout file
        grep_result=$(grep -i "$string" "${species}.domtblout")
        
        # If the string is not found, or if it doesn't contain any of the strings in list_c
        if [[ -z $grep_result ]] || [[ ! "$grep_result" =~ "${list_c[0]}|${list_c[1]}|${list_c[2]}|${list_c[3]}|${list_c[4]}|${list_c[5]}|${list_c[6]}" ]]; then
            echo "$string" >> "${species}_repeats.txt"
        fi
    done < "${species}.txt"
done
```

</details>

<details><summary>remove_repeats.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=filter
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=1G
#SBATCH --mail-type=END
#SBATCH --mail-user=stefan.wnuk@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load seqkit

# Define the list of file pairs
file_pairs=(
    "a_roxburghiana"
    "c_illinoinensis"
    "c_paliurus"
    "j_ailantifolia"
    "j_cinerea"
    "j_hindsii"
    "j_mandshurica"
    "j_microcarpaXregia"
    "j_nigra"
    "j_regia"
    "p_stenoptera"
    "p_strobilacea"
)

# Iterate over each file pair
for pair in "${file_pairs[@]}"; do
    # Extract the file names
    pep_file="${pair}.pep"
    txt_file="${pair}.txt"
    filtered_pep_file="filtered_${pair}.pep"
    
    # Run seqkit grep command
    seqkit grep -v -f "$txt_file" "$pep_file" > "$filtered_pep_file"
done
```

</details>

<details><summary>CAFE.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=cafe
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 32
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=first.last@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
echo -e "\nStart time:"
date

# Load required modules
module load CAFE5/5.1.0

# Set the path to the input file and phylogenetic tree file
input_file=Orthogroups.GeneCount.tsv
tree_file=SpeciesTree_rooted.txt

# Run Cafe5 with the specified parameters 
cafe5 -i $input_file -t $tree_file -o result_03_29

echo -e "\nEnd time:"
date

```

</details>

<details><summary>CAFE_plotter.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=cafeplotter
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=30G
#SBATCH --mail-user=first.last@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

# export PATH="/home/FCAM/mneitzey/miniconda3/bin:$PATH"
# source ~/.bashrc
# source activate cafeplotter-v0.2.0
# export TMPDIR=$HOME/tmp

cafeplotter -i cafe_result_directory -o cafe_plotter --format 'pdf' --fig_width 9
```

</details>

<details><summary>families.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=breakdown_summary
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=first.last@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

summary=result_summary.tsv
list="14 15 16 17 18 19 20 21 22 23 a_roxburghiana c_illinoinensis c_paliurus j_ailantifolia j_cinerea j_hindsii j_mandshurica j_microcarpaXregia j_nigra j_regia p_stenoptera p_strobilacea"

#awk '{if ($5 <= 0.05) {print}}' $summary >> result_summary_sig05.tsv
#awk '{if ($4 > 0 && $5 <= 0.05) {print}}' $summary >> result_summary_sig05_expand.tsv
#awk '{if ($4 < 0 && $5 <= 0.05) {print}}' $summary >> result_summary_sig05_contract.tsv

#awk '{if ($5 <= 0.01) {print}}' $summary >> result_summary_sig01.tsv
#awk '{if ($4 > 0 && $5 <= 0.01) {print}}' $summary >> result_summary_sig01_expand.tsv
#awk '{if ($4 < 0 && $5 <= 0.01) {print}}' $summary >> result_summary_sig01_contract.tsv

awk '{if ($2 == "j_ailantifolia" && $5 <= 0.05) {print}}' $summary >> result_summary_sig05_j_ailantifolia.tsv
awk '{if ($2 == "j_ailantifolia" && $4 > 0 && $5 <= 0.05) {print}}' $summary >> result_summary_sig05_expand_j_ailantifolia.tsv
awk '{if ($2 == "j_ailantifolia" && $4 < 0 && $5 <= 0.05) {print}}' $summary >> result_summary_sig05_contract_j_ailantifolia.tsv

awk '{if ($2 == "j_ailantifolia" && $5 <= 0.01) {print}}' $summary >> result_summary_sig01_j_ailantifolia.tsv
awk '{if ($2 == "j_ailantifolia" && $4 > 0 && $5 <= 0.01) {print}}' $summary >> result_summary_sig01_expand_j_ailantifolia.tsv
awk '{if ($2 == "j_ailantifolia" && $4 < 0 && $5 <= 0.01) {print}}' $summary >> result_summary_sig01_contract_j_ailantifolia.tsv


#for x in $list
#    do 
#        echo $x "0.05 sig"
#        awk -v x="$x" '{if ($2 == x && $5 <= 0.05) { count += 1; }} END { print count; }' $summary
#        echo $x "0.05 sig expand"
#        awk -v x="$x" '{if ($2 == x && $4 > 0 && $5 <= 0.05) { count += 1; }} END { print count; }' $summary
#        echo $x "0.05 sig contract"
#        awk -v x="$x" '{if ($2 == x && $4 < 0 && $5 <= 0.05) { count += 1; }} END { print count; }' $summary
#        echo $x "0.01 sig"
#        awk -v x="$x" '{if ($2 == x && $5 <= 0.01) { count += 1; }} END { print count; }' $summary
#        echo $x "0.01 sig expand"
#        awk -v x="$x" '{if ($2 == x && $4 > 0 && $5 <= 0.01) { count += 1; }} END { print count; }' $summary
#        echo $x "0.01 sig contract"
#        awk -v x="$x" '{if ($2 == x && $4 < 0 && $5 <= 0.01) { count += 1; }} END { print count; }' $summary   
#    done

```

</details>

<details><summary>longest_seqs.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=longest
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=first.last@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

module load seqkit

# Input and output files
in_file="result_summary_sig01_j_ailantifolia.tsv"
out_file="longest_seqs.pep"
search_dir="/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/comparative_genomics/orthofinder/final_pep_files3/OrthoFinder/Results_Mar29_2/Orthogroup_Sequences/"  

# Loop over items in the first column of in_file
cut -f1 "$in_file" | while IFS= read -r item; do
    # Construct path to sequence file
    sequence_file="${search_dir}/${item}.fa"

    # Check if the sequence file exists
    if [ -f "$sequence_file" ]; then
        # Extract the longest sequence from the file and append it to file2
        seqkit sort -lr "$sequence_file" | awk '/^>/ { if (++n == 2) exit } n == 1' >> "$out_file"
    else
        echo "Sequence file '$sequence_file' not found for item '$item'"
    fi
done
```

</details>

<details><summary>entap_config.ini</summary>

```
#-------------------------------
# [ini_instructions]
#When using this ini file keep the following in mind:
#	1. Do not edit the input keys to the left side of the '=' sign
#	2. Be sure to use the proper value type (either a string, list, or number)
#	3. Do not add unecessary spaces to your input
#	4. When inputting a list, only add a ',' between each entry
#-------------------------------
# [configuration]
#-------------------------------
#Specify which EnTAP database you would like to download/generate or use throughout execution. Only one is required.
#    0. Serialized Database (default)
#    1. SQLITE Database
#It is advised to use the default Serialized Database as this is fastest.
#type:list (integer)
data-type=0,
#-------------------------------
# [entap]
#-------------------------------
#Path to the EnTAP binary database
#type:string
entap-db-bin=/core/labs/Wegrzyn/EnTAP/EnTAP_v0.10.8/EnTAP/databases/entap_database.bin
#Path to the EnTAP SQL database (not needed if you are using the binary database)
#type:string
entap-db-sql=
#Path to the EnTAP graphing script (entap_graphing.py)
#type:string
entap-graph=/core/labs/Wegrzyn/EnTAP/EnTAP_v0.10.8/EnTAP/src/entap_graphing.py
#-------------------------------
# [expression_analysis]
#-------------------------------
#Specify the FPKM threshold with expression analysis. EnTAP will filter out transcripts below this value. (default: 0.5)
#type:decimal
fpkm=0.5
#Specify this flag if your BAM/SAM file was generated through single-end reads
#Note: this is only required in expression analysis
#Default: paired-end
#type:boolean (true/false)
single-end=false
#-------------------------------
# [expression_analysis-rsem]
#-------------------------------
#Execution method of RSEM Calculate Expression.
#Example: rsem-calculate-expression
#type:string
rsem-calculate-expression=/isg/shared/apps/EnTAP/0.10.8/EnTAP//libs/RSEM-1.3.3//rsem-calculate-expression
#Execution method of RSEM SAM Validate.
#Example: rsem-sam-validator
#type:string
rsem-sam-validator=/isg/shared/apps/EnTAP/0.10.8/EnTAP//libs/RSEM-1.3.3//rsem-sam-validator
#Execution method of RSEM Prep Reference.
#Example: rsem-prepare-reference
#type:string
rsem-prepare-reference=/isg/shared/apps/EnTAP/0.10.8/EnTAP//libs/RSEM-1.3.3//rsem-prepare-reference
#Execution method of RSEM Convert SAM
#Example: convert-sam-for-rsem
#type:string
convert-sam-for-rsem=/isg/shared/apps/EnTAP/0.10.8/EnTAP//libs/RSEM-1.3.3//convert-sam-for-rsem
#-------------------------------
# [frame_selection]
#-------------------------------
#Select this option if all of your sequences are complete proteins.
#At this point, this option will merely flag the sequences in your output file
#type:boolean (true/false)
complete=false
#Specify the Frame Selection software you would like to use. Only one flag can be specified.
#Specify flags as follows:
#    1. GeneMarkS-T
#    2. Transdecoder (default)
#type:integer
frame-selection=2
#-------------------------------
# [frame_selection-genemarks-t]
#-------------------------------
#Method to execute GeneMarkST. This may be the path to the executable.
#type:string
genemarkst-exe=/isg/shared/apps/EnTAP/0.10.8/EnTAP//libs/gmst_linux_64/gmst.pl
#-------------------------------
# [frame_selection-transdecoder]
#-------------------------------
#Method to execute TransDecoder.LongOrfs. This may be the path to the executable or simply TransDecoder.LongOrfs
#type:string
transdecoder-long-exe=/core/labs/Wegrzyn/easel/updates/entap/dev/test4/EnTAP/TransDecoder-TransDecoder-v5.7.1/TransDecoder.LongOrfs
#Method to execute TransDecoder.Predict. This may be the path to the executable or simply TransDecoder.Predict
#type:string
transdecoder-predict-exe=/core/labs/Wegrzyn/easel/updates/entap/dev/test4/EnTAP/TransDecoder-TransDecoder-v5.7.1/TransDecoder.Predict
#Transdecoder only. Specify the minimum protein length
#type:integer
transdecoder-m=100
#Specify this flag if you would like to pipe the TransDecoder command '--no_refine_starts' when it is executed. Default: False
#This will 'start refinement identifies potential start codons for 5' partial ORFs using a PWM, process on by default.' 
#type:boolean (true/false)
transdecoder-no-refine-starts=false
#-------------------------------
# [general]
#-------------------------------
#Specify the output format for the processed alignments.Multiple flags can be specified:
#    1. TSV Format (default)
#    2. CSV Format
#    3. FASTA Amino Acid (default)
#    4. FASTA Nucleotide (default)
#    5. Gene Enrichment Sequence ID vs. Effective Length TSV (default)
#    6. Gene Enrichment Sequence ID vs. GO Term TSV (default)
#    7. Gene Ontology Terms TSV (default)
#type:list (integer)
output-format=1,3,4,7,
#-------------------------------
# [ontology]
#-------------------------------
# Specify the ontology software you would like to use
#Note: it is possible to specify more than one! Just usemultiple --ontology flags
#Specify flags as follows:
#    0. EggNOG (default)
#    1. InterProScan
#type:list (integer)
ontology=0,
#-------------------------------
# [ontology-eggnog]
#-------------------------------
#Path to the EggNOG SQL database that was downloaded during the Configuration stage.
#type:string
eggnog-sql=/isg/shared/databases/eggnog/4.1/eggnog.db
#Path to EggNOG DIAMOND configured database that was generated during the Configuration stage.
#type:string
eggnog-dmnd=/isg/shared/databases/eggnog/4.1/eggnog4.clustered_proteins.dmnd
#-------------------------------
# [ontology-interproscan]
#-------------------------------
#Execution method of InterProScan. This is how InterProScan is generally ran on your system.  It could be as simple as 'interproscan.sh' depending on if it is globally installed.
#type:string
interproscan-exe=interproscan.sh
#Select which databases you would like for InterProScan. Databases must be one of the following:
#    -tigrfam
#    -sfld
#    -prodom
#    -hamap
#    -pfam
#    -smart
#    -cdd
#    -prositeprofiles
#    -prositepatterns
#    -superfamily
#    -prints
#    -panther
#    -gene3d
#    -pirsf
#    -coils
#    -morbidblite
#Make sure the database is downloaded, EnTAP will not check!
#--protein tigrfam --protein pfam
#type:list (string)
protein=
#-------------------------------
# [similarity_search]
#-------------------------------
#Method to execute DIAMOND. This can be a path to the executable or simply 'diamond' if installed globally.
#type:string
diamond-exe=diamond
#Specify the type of species/taxon you are analyzing and would like alignments closer in taxonomic relevance to be favored (based on NCBI Taxonomic Database)
#Note: replace all spaces with underscores '_'
#type:string
taxon=hadrurus
#Select the minimum query coverage to be allowed during similarity searching
#type:decimal
qcoverage=50
#Select the minimum target coverage to be allowed during similarity searching
#type:decimal
tcoverage=50
#Specify the contaminants you would like to flag for similarity searching. Contaminants can be selected by species or through a specific taxon (insecta) from the NCBI Taxonomy Database. If your taxon is more than one word just replace the spaces with underscores (_).
#Note: since hits are based upon a multitide of factors, a contaminant might end up being the best hit for an alignment. In this scenario, EnTAP will flag the contaminant and it can be removed if you would like.
#type:list (string)
contam=
#Specify the E-Value that will be used as a cutoff during similarity searching.
#type:decimal
e-value=1e-05
#List of keywords that should be used to specify uninformativeness of hits during similarity searching. Generally something along the lines of 'hypothetical' or 'unknown' are used. Each term should be separated by a comma (,) This can be used if you would like to tag certain descriptions or would like to weigh certain alignments differently (see full documentation)
#Example (defaults):
#conserved, predicted, unknown, hypothetical, putative, unidentified, uncultured, uninformative, unnamed
#type:list (string)
uninformative=conserved,predicted,unknown,unnamed,hypothetical,putative,unidentified,uncharacterized,uncultured,uninformative,

```

</details>

<details><summary>entap.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=entap
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G

module load EnTAP/1.0.1

EnTAP --runP --ini $PWD/entap_config.ini -i longest_seqs.pep -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.208.dmnd --threads 16 --out-dir $PWD/filtered_5050_2
```

</details>

<details><summary>eggnog.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=eggnog
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=16G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo hostname

module load python/3.8.1

/home/FCAM/swnuk/.local/bin/emapper.py -m diamond --dmnd_db /core/labs/Wegrzyn/IngaGenome/Eggnog-mapper/eggnog_proteins.dmnd --itype proteins -i longest_seqs.pep -o longest_seqs --data_dir /core/labs/Wegrzyn/IngaGenome/Eggnog-mapper --cpu 16

```

</details>

### Species Tree Inference
<img src="/uploads/21d0470e0904328dcb5f7ca0e08c95e4/Screenshot_2024-06-11_at_1.08.09_PM.png" alt="Alt text" width="1023" height="305" />

### Gene Family Expansions And Contractions
<img src="/uploads/cb6f821c8c2aa8f330813e52b8c496a7/Screenshot_2024-06-11_at_1.09.58_PM.png" alt="Alt text" width="826" height="597" />

### Function Assignment

## 03. Tandem Repeat Analysis
### Overview
Tandem repeats are sequences of DNA where patterns of nucleotides are repeated adjacent to each other. They are a common feature of genomes and can vary in length from a few base pairs to several kilobases. Tandem repeats can be categorized into different types based on the length of the repeat unit, such as microsatellites (short tandem repeats or STRs) and minisatellites. 
### Conda Environment
In order to run StainedGlass( a tool to visualize tandem repeats), a proper conda environment must be created. The following steps were taken to create a miniconda environment.

1. **Start an interactive session**

2. Download Miniconda:
    ```bash
    curl -L -O https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -o Miniconda3-latest-Linux-x86_64.sh
    ```
3. Install Miniconda:
    ```bash
    bash Miniconda3-latest-Linux-x86_64.sh -b -p ~/miniconda3
    ```
4. Set PATH environment variable:
    ```bash
    export PATH=$HOME/miniconda3/bin:$PATH
    ```
5. Log off the cluster and then sign back in.

6. **Start another interactive session**

7. Check for `(base)` before `bash-4.2$`

8. Create a Conda environment:
    ```bash
    conda create --name abinit
    ```
   Note: Replace `abinit` with any desired name for the environment.

9. Create a Conda environment with specific Python version (e.g., Python 3.9):
    ```bash
    conda create -n abinit python=3.9
    conda activate abinit
    ```
10. Confirm `(abinit)` appears before `bash-4.2$`

11. Install `abinit`:
    ```bash
    conda install abinit
    ```
A useful website to help with the creation of a miniconda environment can be found here: https://kb.uconn.edu/space/SH/26079723879/Miniconda+Environment+Set+Up
### ULTRA

#### Running ULTRA

ULTRA, which locates tandemly repetitive areas of the genome, was created by the Travis Wheeler Lab. This tool was used to determine the areas of sequential repeats within the Japanese Walnut genome. More information on set up and running the program can be found [here](https://github.com/TravisWheelerLab/ULTRA).

##### Setting Up cmake

Do each of these lines separately in the command line. Once this is set up you won’t have to do it again.

```bash
wget https://github.com/TravisWheelerLab/ULTRA/archive/refs/tags/0.99.17.tar.gz
tar -xvzf 0.99.17.tar.gz
cd ULTRA-0.99.17/
module load cmake/3.20.2
cmake .
make
```

##### Step One: Initial Run

<details><summary>step one script</summary>

```bash
#!/bin/bash
#SBATCH --job-name=ULTRA.sh
#SBATCH -N 1
#SBATCH -n 5
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=amanda.mueller@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

# Run ULTRA
./ultra -n 5 /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/10_evaluation/rename/renamed_chrom.fasta
```

</details>

##### Step Two: Create .bed File from ULTRA run

<details><summary>step two script</summary>

```bash
#!/bin/bash
#SBATCH --job-name=ultra2bed.sh
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=amanda.mueller@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

python3 ultra2bed.py ULTRA.sh_7653932.out ultrajapwal.bed
```

</details>

##### Step Three: Generating a Fasta Index

<details><summary>step three script</summary>

```bash
#!/bin/bash
#SBATCH --job-name=generatefastaindex
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=amanda.mueller@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load samtools/1.16.1
samtools faidx japwalnut_genome.fa
```

</details>

##### Step Four: Printing Header and Length

<details><summary>step four script</summary>

```bash
#!/bin/bash
#SBATCH --job-name=print_header_length
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=amanda.mueller@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

awk -v OFS='\t' {'print $1,$2'} japwalnut_genome.fa.fai > japwalnut_genomeFile.txt.txt
```

</details>

##### Step Five: Create Genome Sliding Windows

<details><summary>step five script</summary>

```bash
#!/bin/bash
#SBATCH --job-name=Get_Genome_Windows
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=amanda.mueller@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load bedtools/2.29.0
bedtools makewindows -g japwalnut_genomeFile.txt -w 100000 -s 50000 >> japwalnut_genome_window_100kb_50kb-slide.bed
```

</details>

##### Step Six: Create a bedgraph File from the .bed File from the ULTRA run

<details><summary>step six script</summary>

```bash
#!/bin/bash
#SBATCH --job-name=ULTRAbed2bedgraph
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=amanda.mueller@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

inbed=ultrajapwal.bed
prefix=japwalnut_ultra

#######################################
### Print bed col 1-3
#######################################
awk -F'\t' 'BEGIN {OFS = FS} {print $1,$2,$3}' $inbed > ${prefix}_1-3.bed

#######################################
### Merge overlapping repeats
#######################################
module load bedtools/2.29.0
bedtools merge -i ${prefix}_1-3.bed > ${prefix}_1-3_merge.bed

#######################################
### Print length in col 4
#######################################
awk -F'\t' 'BEGIN {OFS = FS} {print $1,$2,$3,$3-$2}' ${prefix}_1-3_merge.bed > ${prefix}_length.bed
```

</details>

##### Step Seven: Sum Up the Repeats in each Window

<details><summary>step seven script</summary>

```bash
#!/bin/bash
#SBATCH --job-name=Sum_Repeats
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=amanda.mueller@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

genome=japwalnut_genomeFile.txt
prefix=japwalnut_ultra
windows=japwalnut_genome_window_100kb_50kb-slide.bed
outbed=japwalnut_genome_window_100kb_50k-slide_sum_repeats.bed

module load bedtools/2.29.0

#######################################
### Make 100kb windows
#######################################
bedtools sort -g $genome -i ${prefix}_length.bed > ${prefix}_length_sort.bed
bedtools map -c 4 -o sum -null 0 -a $windows -b ${prefix}_length_sort.bed > $outbed
```

</details>

##### Step Eight: Sort File for the Top Regions of Tandem Repeats

<details><summary>step eight script</summary>

```bash
#!/bin/bash
#SBATCH --job-name=Sort_for_top_regions
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=amanda.mueller@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

sort -k 4rn japwalnut_genome_window_100

kb_50k-slide_sum_repeats.bed >> japwalnut_genome_window_100kb_50k-slide_sum_repeats_sortH-L.bed
```

</details>


### StainedGlass
#### Installing StainedGlass

1. Make sure you’re in an interactive session and your conda environment is activated.

    ```bash
    conda install -n base -c conda-forge mamba
    mamba create -c conda-forge -c bioconda -n snakemake snakemake=7
    conda activate snakemake
    ```

2. Clone the StainedGlass repository from GitHub:

    ```bash
    git clone https://github.com/mrvollger/StainedGlass.git
    ```

3. Navigate to the StainedGlass directory:

    ```bash
    cd StainedGlass
    ```

4. Run StainedGlass with the provided sample data to ensure everything is working correctly:

    ```bash
    snakemake --use-conda --cores 24 --config sample=small fasta=.test/small.fasta window=2000 nbatch=4 alnthreads=4 mm_f=10000 tempdir=temp
    ```

5. Create stained glass figures:

    ```bash
    snakemake --use-conda --cores 24 make_figures
    ```

These steps were taken before proceeded to run StainedGlass on the top regions of tandem repeats on each genome. The regions were selected based on the ULTRA run.

Example of how to run a section can be seen here :


1. Load samtools version 1.16.1:

```bash
module load samtools/1.16.1
```

2. Index a specific region of the genome (Chr5:1-100000) from the file "renamed_chrom.fasta" and append it to "japwalnut_Chr5_1-100kb.fasta":

```bash
samtools faidx renamed_chrom.fasta Chr5:1-100000 >> japwalnut_Chr5_1-100kb.fasta
```

3. Index the file "/home/FCAM/amueller/StainedGlass/japwalnut_Chr5_1-100kb.fasta" using samtools:

```bash
samtools faidx /home/FCAM/amueller/StainedGlass/japwalnut_Chr5_1-100kb.fasta
```

4. Run snakemake with provided configurations:

```bash
snakemake --use-conda --cores 24 --config sample=Chr5_1-100kb fasta=japwalnut_Chr5_1-100kb.fasta window=1000 nbatch=4 alnthreads=4 mm_f=5000 tempdir=temp
```

5. Update the "Config.yaml" file to match the command line input.

6. Generate figures using snakemake:

```bash
snakemake --use-conda --cores 24 make_figures
```

### Results
![Untitled__18_](/uploads/ac62cb28816275bd6ebdb5a820c765f2/Untitled__18_.png)

Utilizing the ULTRA tool developed by the Travis Wheeler Lab, we pinpointed regions of tandem repeats within the Japanese Walnut genome on a chromosome-by-chromosome basis. Subsequently, we identified and isolated the largest repeat region within each chromosome for further analysis and visualization. Employing the Stained Glass program by Mitchell R. Vollger et al., we rendered these selected genomic sequences into representations, leveraging identity heatmaps. The color gradients within these heatmaps denote the degree of similarity between sequences: red hues signify high similarity, while cooler shades such as blue and purple indicate less  similarity. Absence of color indicates a lack of  correlation between the sequences.

By cross analyzing Stained Glass images between the Japanese walnut and Canadian Butternut genomes, we can see what areas of the genome are heavy in tandem repeats. This information can provide us with possible regions of collapsed coverage, transposable elements,or possible centromere regions. Furthermore, these areas could also give insight on syntenic areas between the two species. Syntenic evidence is further anaylzed using a whole-genome alignment program called Cactus. 
## 04.  Synteny
### Overview
Progressive Cactus is a whole-genome alignment tool designed to align multiple genomes efficiently while incorporating ancestral reconstructions. It follows a progressive-alignment strategy, splitting the large alignment task into smaller sub-alignments using a guide tree. Each subproblem compares a set of ingroup genomes against each other, along with a sample of outgroup genomes. The goal is to reconstruct ancestral assemblies at internal nodes of the guide tree and generate alignments between the children genomes and their ancestral reconstructions.

Using some of the same species used in the gene family analysis, we seeked to find syntenic areas between the genomes to better understand their relationship.

### Cactus Preparation
Genomes from 13 species were selected for synteny analysis based on their proposed relatedness to *Juglans ailantifolia*. Of the species selected, only those with chromosome-scale genome assemblies were chosen to move forward. We then removed any additional scaffolds so all genomes contained only one set of chromosomes. BUSCO and QUAST were run on each genome assembly to assess their completeness and quality before moving forward to run Cactus. We decided to remove the *Juglans microcarpa X regia* genome based on the high N count from the QUAST statistics and because it is a hybrid genome assembly. Repeatmodeler and Repeatmasker were then run on the remaining species to prepare them for Cactus. 

#### BUSCO Analysis
| Species                   | Complete | Single | Duplicate | Fragment | Missing |
|---------------------------|----------|--------|-----------|----------|---------|
| *Alfaropsis roxburghiana*  | 99.20%   | 80.40% | 18.80%    | 0.40%    | 0.50%   |
| *Carya illinoinensis*       | 98.40%   | 75.70% | 22.70%    | 1.60%    | 0.00%   |
| *Cyclocarya paliurus*       | 95.30%   | 72.20% | 23.10%    | 1.20%    | 3.50%   |
| *Juglans ailantifolia*      | 98.90%   | 91.40% | 7.50%     | 0.50%    | 0.60%   |
| *Juglans cinerea*           | 98.90%   | 71.80% | 27.10%    | 1.20%    | 0.10%   |
| *Juglans mandshurica*       | 99.60%   | 72.50% | 27.10%    | 0.40%    | 0.00%   |
| *Juglans microcarpa x regia*| 99.20%   | 74.90% | 24.30%    | 0.80%    | 0.00%   |
| *Juglans nigra*             | 99.20%   | 73.70% | 25.50%    | 0.80%    | 0.00%   |
| *Juglans regia*             | 99.30%   | 71.90% | 27.50%    | 0.80%    | 0.10%   |
| *Platycarya strobilacea*    | 98.40%   | 80.40% | 18.00%    | 0.40%    | 1.20%   |

#### QUAST Analysis
| Species                   | # of Contigs | Largest Contig (bp) | Total Length (bp) | GC Content % | N50 (bp) | L50 | #Ns per 100kb |
|---------------------------|--------------|---------------------|-------------------|--------------|----------|-----|---------------|
| *Alfaropsis roxburghiana*  | 16           | 74945250            | 864690398         | 35.25        | 54380750 | 7   | 10.28         |
| *Carya illinoinensis*       | 16           | 58059119            | 674272338         | 36.3         | 44708330 | 7   | 26.74         |
| *Cyclocarya paliurus*       | 16           | 46582194            | 553868155         | 36.17        | 37075707 | 7   | 14.19         |
| *Juglans ailantifolia*      | 16           | 54008184            | 526265674         | 36.46        | 34414504 | 7   | 6.25          |
| *Juglans cinerea*           | 16           | 53338669            | 539433174         | 36.64%       | 34859067 | 7   | 5.32          |
| *Juglans mandshurica*       | 16           | 52477553            | 528151479         | 36.49        | 35382463 | 7   | 3.22          |
| *Juglans microcarpa x regia*| 16           | 49856174            | 522780668         | 36.26        | 35629462 | 7   | 824.48        |
| *Juglans nigra*             | 16           | 50134109            | 530236200         | 36.36        | 35148204 | 7   | 0.68          |
| *Juglans regia*             | 16           | 52418484            | 545778456         | 36.08        | 37114715 | 7   | 106.57        |
| *Platycarya strobilacea*    | 15           | 63051394            | 676806548         | 34.54        | 45533441 | 7   | 2.33          |


## Transposable Element Curation and Analysis

### Overview

Transposable elements are regions within the genome that can, through various different mechanisms, move and/or self-replicate throughout the genome. Due to their nature as constantly evolving genetic elements that frequently mutate and diverge over evolutionary time, automated identification of these transposable elements has proven to be difficult, and typically requires a significant amount of manual labour to create and curate a library of transpoable element families for a genome.

### Setting up work environment

The main programs used for transposable element (TE) curation were RepeatModeler and RepeatMasker. While both programs are installed on the Xanadu cluster as modules, usage of the scripts included within these programs requires a manual directory installation. A simple conda or similar installation will not work, as the included scripts require access to module files not accessible through a conda installation.

Instead, use:
```
mkdir Curation
cd Curation
wget https://github.com/Dfam-consortium/RepeatModeler/archive/refs/tags/2.0.4.tar.gz
tar -xzvf 2.0.4.tar.gz
wget https://www.repeatmasker.org/RepeatMasker/RepeatMasker-4.1.6.tar.gz
tar -xzvf RepeatMasker-4.1.6.tar.gz
```

Both programs come with multiple other required dependencies, which are most easily installed through conda:

```
conda create -n curation
conda activate curation
conda install RepeatModeler
```

⚠️ **Notice:**
As both programs have multiple dependencies that will be automatically installed, installation of Libmamba or similar is recommended prior to installation. RepeatMasker will be automatically installed with RepeatModeler.


Install RepeatMasker and RepeatModeler through the downloaded packages:

```
cd RepeatMasker
perl ./configure
cd ../RepeatModeler
cd RepeatModeler-2.0.4
perl ./configure
```

After each `perl ./configure`, follow steps listed for installation. RepeatMasker will prompt to install a limited database, which will then download with '1'. RepeatModeler will prompt to ask for the location of dependencies, which should automatically fill in from the conda installation. Importantly, replace the auto-filled RepeatMasker directory with your manually installed RepeatMasker directory.
⚠️ **Note:**
If running into a `Can't locate *.pm in @INC` error later in the process, make sure RepeatModeler has been correctly configured with the manually installed RepeatMasker as the selected path.

stkToFa.py: A later step will require the following Biopython script and an installation of Biopython.

<details>
  <summary>stkToFa.py</summary>
  
```from Bio import SeqIO
 import sys
 import os

 records = SeqIO.parse(sys.argv[1], "stockholm")
 # First output are the sequences in stockholm format;
 # this file will be deleted at the end of the process
 output1 = sys.argv[2]

 #Convert and count records in output1 (stockholm)
 count = SeqIO.write(records, output1, "fasta")

 output2 = open(sys.argv[3],'w')
 for record in SeqIO.parse(output1,"fasta"):
    
    # Write records to file
    output2.write(">"+record.id+"\n"+str(record.seq.ungap("-"))+"\n")

 print("Converted %i sequences" % count)

 #Remove the gapped sequence file
 os.remove(sys.argv[2])
 ```
</details>

### RepeatMasker output

In order to identify repetetive sequences of the genome as well as obtaining an initial classification of repeat families, run RepeatModeler and RepeatMasker using the following script:

```
singularity exec /core/projects/EBP/software/TEtools/dfam-tetools-latest.sif BuildDatabase -name "rm_database" $genome
singularity exec /core/projects/EBP/software/TEtools/dfam-tetools-latest.sif RepeatModeler -threads 30 -database rm_database
singularity exec /core/projects/EBP/software/TEtools/dfam-tetools-latest.sif RepeatMasker -dir repeatmasker_out -pa 16 -lib RM_*/consensi.fa.classified -gff -a -noisy -xsmall $genome
```

This produces directory `repeatmasker_out`, which will include the output file `filename.fasta.out`.
Build summary of the RepeatMasker output using the buildsummary.pl script found in the RepeatMasker/util directory:

```
/isg/shared/apps/RepeatMasker/4.1.2/RepeatMasker/util/buildSummary.pl renamed_chrom.fasta.out >> renamed_chrom.fasta.out.summary
```

The summary file should include (1) detected classifications, (2) number of times each appears in the genome, (3) total size in base pairs, and (4), percentage of genome masked:

```
Repeat Classes
==============
Total Sequences: 16
Total Length: 526265674 bp
Class                  Count        bpMasked    %masked
=====                  =====        ========     =======
DNA                    201          149713       0.03% 
    CMC-EnSpm          5184         8007697      1.52% 
    MULE-MuDR          5511         2550133      0.48% 
    Maverick           145          38693        0.01% 
    PIF-Harbinger      1754         1035062      0.20% 
    Zisupton           1115         178905       0.03% 
    hAT-Ac             7442         5276559      1.00% 
    hAT-Tag1           331          378422       0.07% 
    hAT-Tip100         1486         612986       0.12% 
LINE                   --           --           --   
    L1                 50733        32648167     6.20% 
    L1-Tx1             100          17242        0.00% 
    L2                 462          39037        0.01% 
    RTE-BovB           108          19479        0.00% 
LTR                    74           58054        0.01% 
    Caulimovirus       3896         3101950      0.59% 
    Copia              36952        25351512     4.82% 
    ERV1               509          416141       0.08% 
    ERVK               116          130446       0.02% 
    Gypsy              31792        29262936     5.56% 
RC                     --           --           --   
    Helitron           2833         2178227      0.41% 
SINE                   --           --           --   
    5S                 568          69026        0.01% 
    U                  36           6743         0.00% 
Unknown                645776       164725123    31.30% 
                      ---------------------------------
    total interspersed 797124       276252253    52.49%

Low_complexity         23520        1140793      0.22% 
Simple_repeat          163048       6416638      1.22% 
rRNA                   31           3889         0.00% 
snRNA                  295          42138        0.01% 
tRNA                   47           2419         0.00% 
---------------------------------------------------------
Total                  984065       283858130    53.94%
```

Visualized in Excel:

![beforecuration](/Figures/beforecuration.png)

Sort summary output by percentage of genome masked:

```
sed '1,/Repeat[[:space:]]Stats/d' renamed_chrom.fasta.out.summary | \
                sed '1,/================/d' | \
                sed '/--------------------------------------------------------/Q' | \
                sed -e 's/^ *//g' | \
                sed 's/ \+ /\t/g' | \
                sort -k 4rn >> \
```

<b>EXPLAIN</b> Convert genome to 2bit file format and generate seed alignments using .align file produced from RepeatMasker output and generateSeedAlignments.pl from the RepeatModeler/util directory: 

```
fasta=/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/10_evaluation/length/chromosome_contigs.fasta
align=/core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/annotation/04_repeat_modeler/repeatmasker_out/renamed_chrom.fasta.align

faToTwoBit $fasta out.2bit
cd stks
../../generateSeedAlignments.pl -assemblyFile ../out.2bit $align
```

<b>Future plans :</b> 

After manual curation, rerun RepeatMasker using curated families as input.

Add US Butternut statistics for comparison.

With candidate genes and their positions in the genome, look at repeat activity in the region.

Synteny and alignment comparisons with US Butternut.

Compare divergence and predicted age for shared families between Japanese walnut and Butternut.

See how this matches with gene expansion and methylation patterns.